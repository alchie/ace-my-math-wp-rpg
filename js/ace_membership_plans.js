(function($) {
    $(document).ready(function($) {
        $('.delete-plan').on('click', function() {
            if( confirm("You are about to permanently delete the selected items. 'Cancel' to stop, 'OK' to delete.") ) {
                return true;
            }
            return false;
        });
        $('#level-tabs').tabs();
        $( ".accordion" ).accordion({ collapsible: true, heightStyle: "content" });
        //$( "#lessons-chapters-accordion" ).accordion({ collapsible: true, heightStyle: "content" });
        $('.level-checkbox').on('click', function() {
            var on = $(this).prop('checked'), id = $(this).val();
            if( on ) {
                //$('#lesson-level-nav-'+id).show();
                //$('#lesson-level-tab-'+id).show(); 
                //$('.lesson-checkbox-level-'+id).prop('checked', true);     
            } else {
                //$('#lesson-level-nav-'+id).hide();
                //$('#lesson-level-tab-'+id).hide();
                //$('.lesson-checkbox-level-'+id).prop('checked', false);
            }
        });
    });
})(jQuery);
