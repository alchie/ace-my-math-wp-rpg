(function($) {
    $(document).ready(function($) {
        var quiz_item_edit = function() {
            var id = $(this).attr('data-id');
            var display = $('#quiz-item-settings-' + id).css('display');
            if( display == 'none' ) {
                $('#quiz-item-settings-' + id).slideDown();
            } else {
                $('#quiz-item-settings-' + id).slideUp();
            }
        },
        quiz_item_delete = function() {
            var id = $(this).attr('data-id');
            if( confirm('Are you sure you want to delete this?') ) {
                $('#video-quiz-' + id).slideUp( function() { $(this).remove() } );
            }
        },
        input_popup_title = function() {
            var id = $(this).attr('data-id');
            $('#popupTitle-' + id ).html( $(this).val() + ' <small>('+ $('.choice-type-'+id+':checked').val() +')</small>' );
        },
        input_popup_time = function() {
            var id = $(this).attr('data-id');
            $('#popupTime-' + id ).text( $(this).val() );
        },
        quiz_choice_delete = function() {
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var choice = $(this).attr('data-choice');
            $('#quiz-choice-'+index+'-'+choice).remove();
        },
        quiz_input_delete = function() {
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var n = $(this).attr('data-n');
            $('#quiz-input-'+index+'-'+n).remove();
        },
        add_quiz_item = function() {
            var id = $(this).attr('data-id');
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            
            $('#' + id + '-sortable').append('<li id="'+id+'-quiz-'+index+'">' 
+            '<div class="quiz-item-bar">'
 +               '<div class="quiz-item-handle">'
 +                   '<strong><span id="popupTime-'+index+'"></span> - <span id="popupTitle-'+index+'">New Item</span></strong> '
 +                   '<div class="item-actions">'
 +                       '<a href="javascript:void(0);" class="item-edit quiz-item-edit" data-id="'+index+'" id="quiz-item-edit-'+index+'">'
 + '<div class="dashicons dashicons-edit"></div></a>'
 +                       '<a href="javascript:void(0);" class="item-delete quiz-item-delete" data-id="'+index+'" id="quiz-item-delete-'+index+'">'
 + '<div class="dashicons dashicons-no-alt"></div></a>'
 +                   '</div>'
 +               '</div>'
 +           '</div>'
 +           '<div id="quiz-item-settings-'+index+'" class="quiz-item-settings" style="display:block;">'
  +                '<p>'
 +                   '<label>Popup Time (MM:SS)<br>'
 +                       '<input type="text" name="'+field_id+'['+index+'][popupTime]" value="" class="block input-popup-time" id="input-popup-time-'+index+'" data-id="'+index+'">'
 +                   '</label>'
 +               '</p>'
 +               '<p>'
 +                   '<label>Question<br>'
 +                       '<input type="text" name="'+field_id+'['+index+'][popupTitle]" value="" class="block input-popup-title" id="input-popup-title-'+index+'" data-id="'+index+'">'
 +                   '</label>'
 +               '</p>'
                 +'<div class="choice">'
                +'<h3><label><input id="choice-type-'+index+'" class="choice-type choice-type-'+index+'" data-class="'+field_id+'-'+index+'-choice-type-box" data-id="'+field_id+'-'+index+'-choice" type="radio" value="choices" name="'+field_id+'['+index+'][input_type]"> Multiple Choice</label>'
                + ' <label><input id="choice-type-'+index+'" class="choice-type choice-type-'+index+'" data-class="'+field_id+'-'+index+'-choice-type-box" data-id="'+field_id+'-'+index+'-input" type="radio" value="input" name="'+field_id+'['+index+'][input_type]"> Input Box</label></h3>'
                +'<div id="'+field_id+'-'+index+'-choice" style="display:none" class="'+field_id+'-'+index+'-choice-type-box"><h4>Choices</h4><table id="choices-'+field_id+'-'+index+'">'
                    +'<thead>'
                      +'  <tr>'
                      +'      <th></th> '
                      +'      <th width="50%">Label</th>'
                      +'      <th></th>                            '
                      +'  </tr>'
                    +'</thead>'
                    +'<tbody>'
                +'</tbody>'
                +'</table>            '
                +'<a href="javascript:void(0);" data-id="'+id+'" data-field-id="'+field_id+'" data-index="'+index+'" data-choice="0" class="add-choice-item button button-primary button-large" id="add-choice-item-'+index+'">Add Choice</a></div>'

+ '<div id="'+field_id+'-'+index+'-input" style="display:none" class="'+field_id+'-'+index+'-choice-type-box">'
+ '<h4>Input Boxes</h4>'
+ '  <table id="inputbox-'+field_id+'-'+index+'">'
  + '                  <thead>'
     + '                   <tr>'
        + '                    <th></th> '
           + '                 <th>Box Type</th> '
              + '              <th>Label / Placeholder</th>'
                 + '           <th width="50%">Correct Answer</th>                            '
                    + '    </tr>'
                    + '</thead>'
                    + '<tbody>'
                + '</tbody>'
                + '</table>'
+ '<a href="javascript:void(0);" data-id="'+id+'" data-field-id="'+field_id+'" data-index="'+index+'" data-n="0" class="add-input-item button button-primary button-large" id="add-input-item-'+index+'">Add Box</a>'
+ '</div>'
                +'</div>'
 +               '<br class="clear" />'
 +           '</div>'
 +           '</li>');
            $('.choice-type-' + index ).click( choice_type );
            $('#quiz-item-edit-' + index).click(quiz_item_edit);
            $('#quiz-item-delete-' + index).click(quiz_item_delete);
            $('#input-popup-title-' + index).blur(input_popup_title);
            $('#input-popup-time-' + index).blur(input_popup_time);
            $('#add-choice-item-' + index).click( add_choice_item );
            $('#add-input-item-' + index).click( add_input_item );
            $(this).attr('data-index', parseInt(index)+1);
        },
        add_choice_item = function() {
            var id = $(this).attr('data-id');
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var choice = $(this).attr('data-choice');
            $('#choices-'+field_id+'-'+index+' tbody').append('<tr id="quiz-choice-'+index+'-'+choice+'">'
                        +'<td><a id="quiz-choice-delete-'+index+'-'+choice+'" href="javascript:void(0);" class="choice-delete quiz-choice-delete" data-field-id="'+field_id+'" data-index="'+index+'" data-choice="'+choice+'"><div class="dashicons dashicons-no-alt"></div></a></td>'
                        +'<td><input type="text" class="block input-popup-title" name="'+field_id+'['+index+'][choices]['+choice+'][choice]" value=""></td>'
                        +'<td><input type="checkbox" name="'+field_id+'['+index+'][choices]['+choice+'][correct]" value="1">Correct Answer?</td>'
                    + '</tr>');
                    $('#quiz-choice-delete-'+index+'-'+choice).click(quiz_choice_delete);
                    
                    $(this).attr('data-choice', parseInt(choice)+1);
        },
        add_input_item = function() {
            var id = $(this).attr('data-id');
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var n = $(this).attr('data-n');
            $('#inputbox-'+field_id+'-'+index+' tbody').append('<tr id="quiz-input-'+index+'-'+n+'">'
                        +'<td><a id="quiz-input-delete-'+index+'-'+n+'" href="javascript:void(0);" class="input-delete quiz-input-delete" data-field-id="'+field_id+'" data-index="'+index+'" data-n="'+n+'"><div class="dashicons dashicons-no-alt"></div></a></td>'
                        +'<td><select data-n="'+n+'" data-field-id="'+field_id+'" data-index="'+index+'" id="input-box-type-'+index+'-'+n+'" name="'+field_id+'['+index+'][input]['+n+'][type]"><option SELECTED>input</option><option>label</option><option>dropdown</option></select></td>'
                        +'<td><input type="text" class="block input-popup-title" name="'+field_id+'['+index+'][input]['+n+'][label]" value=""></td>'
                       +'<td  class="answers-choices"><input type="text" class="block input-popup-title" name="'+field_id+'['+index+'][input]['+n+'][answer]" value=""></td>'
                    + '</tr>');
                    $('#quiz-input-delete-'+index+'-'+n).click(quiz_input_delete);
                    $('#input-box-type-'+index+'-'+n).click(input_box_type_change);
                    $(this).attr('data-n', parseInt(n)+1);
            },
        choice_type = function() {
            var data_id = $(this).attr('data-id'), data_class = $(this).attr('data-class');
            $('.'+data_class).each(function() {
                if( $(this).css('display') == 'block' ) {
                    $(this).hide();
                }
                });
            $('#'+data_id).show();
            },
        input_box_type_change = function() {
            var type= $(this).val();
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var input_n = $(this).attr('data-n');
            if( type == 'input' | type == 'label' ) {
                $('#quiz-input-'+index+'-'+input_n+' .answers-choices').html('<input type="text" class="block input-popup-title" name="'+field_id+'['+index+'][input]['+input_n+'][answer]" value="">');
            }
            if( type == 'dropdown' ) {
                $('#quiz-input-'+index+'-'+input_n+' .answers-choices').html('<div id="dropdown-choices-'+index+'-'+input_n+'" class="dropdown-choices-box"></div><button id="dropdown-choices-add-'+index+'-'+input_n+'" class="button button-primary button-large dropdown-choices-add" type="button" data-choice="0" data-n="'+input_n+'" data-field-id="'+field_id+'" data-index="'+index+'">Add Choice</button>');
                $('#dropdown-choices-add-'+index+'-'+input_n).click(dropdown_choices_add);
            }
        },
        dropdown_choices_add = function() {
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var input_n = $(this).attr('data-n');
            var choice_n = $(this).attr('data-choice');
            $('#dropdown-choices-' + index + '-' + input_n ).append('<div id="input-dropdown-choice-'+index+'-'+input_n+'-'+choice_n+'" class="input-dropdown-choice"><input type="text" class="" name="'+field_id+'['+index+'][input]['+input_n+'][choices]['+choice_n+'][option]" value=""><input type="radio" name="'+field_id+'['+index+'][input]['+input_n+'][answer]" value="1"> Correct? <button id="dropdown-choices-close-'+index+'-'+input_n+'-'+choice_n+'" class="btn btn-success dropdown-choices-close" type="button" data-n="'+input_n+'" data-field-id="'+field_id+'" data-index="'+index+'" data-choice="'+choice_n+'">x</button></div>');
            $(this).attr('data-choice', parseInt(choice_n)+1);
            $('#dropdown-choices-close-'+index+'-'+input_n+'-'+choice_n).click( dropdown_choices_close );
        },
        dropdown_choices_close = function() {
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var input_n = $(this).attr('data-n');
            var choice_n = $(this).attr('data-choice');
            $('#input-dropdown-choice-'+index+'-'+input_n+'-'+choice_n).remove();
        },
        input_dropdown_choice_textbox = function() {
            var index = $(this).attr('data-index');
            var field_id = $(this).attr('data-field-id');
            var input_n = $(this).attr('data-n');
            var choice_n = $(this).attr('data-choice');
            $('#input-dropdown-choice-radio-'+index+'-'+input_n+'-'+choice_n).val( $(this).val() );
        };
        $('#add-quiz-item').click(add_quiz_item);
        $('.choice-type').click( choice_type );
        $('.quiz-item-edit').click(quiz_item_edit);
        $('.quiz-item-delete').click(quiz_item_delete);
        $('.input-popup-title').blur(input_popup_title);
        $('.input-popup-time').blur(input_popup_time);   
        $('.quiz-choice-delete').click(quiz_choice_delete);    
        $('.quiz-input-delete').click(quiz_input_delete);    
        $('.add-choice-item').click(add_choice_item);
        $('.add-input-item').click( add_input_item );
        $('.input-box-type').change( input_box_type_change );
        $('.input-dropdown-choice-textbox').keyup( input_dropdown_choice_textbox );
        $('.dropdown-choices-add').click( dropdown_choices_add );
        $('.dropdown-choices-close').click( dropdown_choices_close );
    });
})(jQuery);
