<?php

if ( ( ! class_exists('AceMyMathCharacters') ) && ( class_exists('AceMyMathRPG')) ) 
{
    class AceMyMathCharacters {
        
        function __construct() {
            // session
            add_action('init', array(&$this, 'start_session') );
            add_action('wp_logout', array(&$this, 'destroy_session') );
            
            
            add_action('admin_menu', array(&$this, 'admin_menu') );
            
            // create character
            add_shortcode('create_character', array(&$this, 'create_character_shortcode') );
            add_action('init', array(&$this, 'create_character_save') );
            
            // select character
            add_shortcode('select_character', array(&$this, 'select_character_shortcode') );
            add_action('init', array(&$this, 'select_character_save') );
            
            // show selected character
            add_action('wp_footer', array(&$this, 'show_selected_character') );            
            
        }
        
        function admin_menu() {
            add_submenu_page( 'acemymath', 'Ace My Math Characters', 'Characters', 'manage_options', 'acemymath-characters', array(&$this, 'admin_page') ); 
        }
        
        function admin_page() {
	        echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		        echo '<h2>Characters</h2>';
	        echo '</div>';
        }
        
        function create_character_shortcode($atts, $content=NULL) {

        echo '<form method="post"><div class="row">';
        for($i=1; $i < 7; $i++) {
            echo '<div class="col-md-2"><label style="cursor:pointer;"><img src="'.$this->assets_folder('images', 'character'.$i.'.png').'" width="">';
            echo '<center><input type="radio" name="character_id" value="'.$i.'" /></center></label>';
            echo '</div>';
        }
        echo '</div><div class="row"><div class="col-md-12"><input type="hidden" name="action" value="ace_create_character" /><input type="hidden" name="nonce" value="'.wp_create_nonce( 'create_character' ).'" />';
        echo '<input type="text" name="character_name" value="" placeholder="Character Name" /><input type="submit" value="Create Character"/>';
        echo '</div></div></form>';
        }
        
        function create_character_save() {
            if( ( isset($_POST['action']) && $_POST['action'] == 'ace_create_character') 
            && ( isset($_POST['nonce']) && (wp_verify_nonce($_POST['nonce'], 'create_character') ) )
            ) {
                $keys = array('character_id', 'character_name');
                $proceed = true;
                
                $ins_arr = array();
                foreach($keys as $k) {
                    $proceed = $this->__NE($k);
                        if( $proceed ) $ins_arr[$k] = $_POST[$k];
                }
                
                if($proceed) {
                
                    global $wpdb;
                    $current_user = wp_get_current_user();
                    $table_name = $wpdb->prefix . "ace_rpg_characters";
                    $ins_arr['user_id'] = $current_user->ID;
                    $rows_affected = $wpdb->insert($table_name, $ins_arr);
                    if($rows_affected) {
                        // message here
                    }
                }
                
            }
        }
        
        function select_character_shortcode($atts, $content=NULL) {
            if ( is_user_logged_in() ) {
                global $wpdb;
                $current_user = wp_get_current_user();
                $table_name = $wpdb->prefix . "ace_rpg_characters";
                $user_characters = $wpdb->get_results(  "SELECT * FROM `$table_name` WHERE `user_id` = " . $current_user->ID );
                
                
                echo "<div class=\"row\">";
                foreach($user_characters as $uchar) {
                    echo "<div class=\"col-md-3\" style=\"margin-top:10px\"><form method=\"POST\">";
                    echo '<img src="'.$this->assets_folder('images', 'character'. $uchar->character_id .'.png').'">';
                    echo "<input type=\"hidden\" name=\"action\" value=\"ace_select_character\">";
                    echo "<input type=\"hidden\" name=\"nonce\" value=\"".wp_create_nonce( 'select_character_' . $uchar->ID )."\">";
                    echo "<input type=\"hidden\" name=\"character_id\" value=\"{$uchar->ID}\">";
                    echo "<input type=\"hidden\" name=\"character_avatar\" value=\"{$uchar->character_id}\">";
                    echo "<input type=\"hidden\" name=\"character_name\" value=\"{$uchar->character_name}\">";
                    echo "<input type=\"submit\" class=\"form-control\" value=\"Play with {$uchar->character_name}\">";                    
                    echo "</form></div>";
                }
                echo "</div>";
                
            }
        }
        
        function select_character_save() {
        
            if( ( isset($_POST['action']) && $_POST['action'] == 'ace_select_character') 
            && ( isset($_POST['nonce']) && (wp_verify_nonce($_POST['nonce'], 'select_character_' . $_POST['character_id']) ) )
            ) {
            $_SESSION['character_id'] = $_POST['character_id'];
            $_SESSION['character_avatar'] = $_POST['character_avatar'];
            $_SESSION['character_name'] = $_POST['character_name'];
           }
        }
        
        function show_selected_character() {
            if( (isset( $_SESSION['character_id'] ) && $_SESSION['character_id'] != '' ) 
            && (isset( $_SESSION['character_avatar'] ) && $_SESSION['character_avatar'] != '' ) 
           ) {
                echo '<div class="current_character" style="position:fixed;bottom:0;right:0;"><img src="'.$this->assets_folder('images', 'character'.$_SESSION['character_avatar'].'.png').'">';
                echo '</div>';
            }
        }
        
        function assets_folder($inner, $file) {
            return implode( "/", array( get_bloginfo('home'), "assets" , $inner , $file) );
        }
        
        function __NE($key) {
            if( isset($_POST[$key]) && $_POST[$key] == '') {
                return false;
            }
            return true;
        }
        
        function start_session() {
           if( !session_id() ) {
                session_start();
            }
        }
        
        function destroy_session() {
            session_start();
            unset($_SESSION['character_id']);
            unset($_SESSION['character_avatar']);
            unset($_SESSION['character_name'] );
            session_destroy();
        }
    }

}
