<?php

if ( ( ! class_exists('AceMyMathSettings') ) && ( class_exists('AceMyMathRPG')) ) 
{
    class AceMyMathSettings {
        function __construct() {
            add_action('admin_menu', array(&$this, 'admin_menu') );            
        }
        
        function admin_menu() {
            add_submenu_page( 'acemymath', 'Settings', 'Settings', 'manage_options', 'acemymath-settings', array(&$this, 'admin_page') ); 
        }
        function admin_page() {
	        echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		        echo '<h2>Settings</h2>';
	        echo '</div>';
        }
    }


}
