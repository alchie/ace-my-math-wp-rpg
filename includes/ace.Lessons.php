<?php
/* 
Name: AceMyMath Lessons Class
Filename: ace.Lessons.php
*/
if ( ( ! class_exists('AceMyMathLessons') ) && ( class_exists('AceMyMathRPG')) ) 
{
    class AceMyMathLessons {
        private $post_type_id = 'lesson';
        private $lesson = false;
        private $tax_level = false;
        
        function __construct() {
            $this->post_type_init();
            $this->taxonomy_init();
            $this->term_meta_init();
            $this->metabox_init();
           
        }
        
        function post_type_init() {
            if ( class_exists('Custom_Post_Type') ) {
                $this->lesson = new Custom_Post_Type;                
                $this->lesson->set_id( $this->post_type_id )->set_name('Lesson')->set_plural('Lessons')->set_position(1001)->add_support('editor')->add_support('page-attributes')->init();
            
            }
        }
        
        function taxonomy_init() {
            if ( class_exists('Custom_Taxonomy') && ($this->lesson) ) {
                    $this->tax_level = new Custom_Taxonomy;
                    $this->tax_level->set_id('level')->set_name('Level')->set_plural('Levels')->set_menu_name('Levels')->set_post_type( $this->post_type_id )->init();
                    
                    $chapter = new Custom_Taxonomy;
                    $chapter->set_id('chapter')->set_name('Chapter')->set_plural('Chapters')->set_menu_name('Chapters')->set_post_type( $this->post_type_id )->init();

                }
        }
        function term_meta_init() {
            if ( class_exists('Custom_Term_Meta') && ($this->tax_level) ) {
                        $level_meta = new Custom_Term_Meta('level');
                        $level_meta->add_field( array('label' => 'Icon', 'meta_key'=> 'level_icon', 'meta_value'=> '', 'desc'=>'Icon to be used in level') );
                        $level_meta->init();
             }
        }
        function metabox_init() {
        
             if ( class_exists('Custom_Metabox') && ($this->lesson) ) {
             
                    $video = new Custom_Metabox( $this->post_type_id , 'video', 'Video Settings', 'advanced', 'high');
                    
                    //$video->add_tab( array( 'id' => 'video-quiz', 'label' => 'Quiz Settings' ) );                    
                    //$video->add_field( array('tab' => 'video-quiz', 'label' => '','desc' => '','id' => 'lesson_video_quizzes','type' => 'video-lesson-quiz','default' => '') );                    
                    
                    //$video->add_tab( array( 'id' => 'video-settings', 'label' => 'Video Settings' ) );                    
                    $video->add_field( array('tab' => 'video-settings', 'label' => 'Video URL','desc' => 'Video URL Source','id' => 'lesson_video_url','type' => 'text','default' => '') );
                    $video->add_field( array('tab' => 'video-settings', 'label' => 'Video Duration','desc' => 'Video Duration','id' => 'lesson_video_duration','type' => 'text','default' => '') );
                    
                    $video->init();
                    
                    $quiz = new Video_Lesson_Quiz_Metabox( $this->post_type_id , 'video-quiz', 'Video Quizzes', 'advanced', 'high' );
                    $quiz->add_field(array('id'=>'lesson_video_quizzes'));
                    $quiz->init();
                    
                    $exercise = new Custom_Metabox( $this->post_type_id , 'exercise', 'Exercise Filename', 'side', 'high');
                    $exercise->add_field( array('label' => 'Exercise Filename','desc' => 'Exercise Filename','id' => 'exercise_filename','type' => 'text','default' => '') );
                    $exercise->init();
                }
        
        }
        
    }
    
    function get_ace_lessons($level, $chapter) {
        global $wpdb, $current_user;
        get_currentuserinfo();
        $tax_query = new WP_Tax_Query( array(
		    array(
			    'taxonomy' => 'level',
			    'field' => 'term_id',
			    'terms' => $level
		    ),
		    array(
			    'taxonomy' => 'chapter',
			    'field' => 'term_id',
			    'terms' => $chapter
		    )
	    ) );
	    
	    $clauses = $tax_query->get_sql( $wpdb->posts, 'ID' );
        $sql = "SELECT {$wpdb->prefix}posts.*, (SELECT `date_taken` FROM  `{$wpdb->prefix}ace_lesson_progress` as ace WHERE ace.lesson_id = {$wpdb->prefix}posts.ID AND ace.user_id = {$current_user->ID} ) as date_taken FROM {$wpdb->prefix}posts {$clauses['join']}  WHERE 1=1 {$clauses['where']} AND {$wpdb->prefix}posts.post_type = 'lesson' AND {$wpdb->prefix}posts.post_status = 'publish' GROUP BY {$wpdb->prefix}posts.ID ORDER BY {$wpdb->prefix}posts.menu_order ASC";
        
        return $wpdb->get_results( $sql );
    }
    
}
