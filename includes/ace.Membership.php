<?php

if ( ( ! class_exists('AceMyMathMembership') ) && ( class_exists('AceMyMathRPG')) ) 
{
    class AceMyMathMembership {
        function __construct() {
            //add_action('init', array(&$this, 'restrict_direct_access_wplogin') ); 
            //add_action('login_init', array(&$this, 'restrict_direct_access_wplogin') );
            add_action('after_setup_theme', array(&$this, 'disable_admin_toolbar') );
            add_action('login_form', array(&$this, 'redirect_login_to_front_page') );
            add_action('wp_logout', array(&$this, 'redirect_logout_to_front_page') );    
            add_action( 'admin_init', array(&$this,'disable_access_adminpanel') );                    
            add_action('login_init', array(&$this, 'no_access_wplogin') );    
            add_action('init', array(&$this, 'create_ace_account') );
            add_action('init', array(&$this, 'pay_ace_accounts') );
            add_action('init', array(&$this, 'add_ace_student') );
            add_action('init', array(&$this, 'manage_ace_student') );
            add_action('init', array(&$this, 'delete_ace_student') );   
            add_action('init', array(&$this, 'activate_ace_student') );     
            add_action('init', array(&$this, 'renew_ace_account') );   
            add_action('init', array(&$this, 'update_my_account') );     
            add_action('init', array(&$this, 'login_ace_account') );
            add_action('init', array(&$this, 'upgrade_ace_student') );
            add_action('init', array(&$this, 'add_membership_session') );
            add_action('show_user_profile', array(&$this, 'profile_edit_action') );
            add_action('edit_user_profile', array(&$this, 'profile_edit_action') );
            add_action('personal_options_update', array(&$this, 'profile_update_action') );
            add_action('edit_user_profile_update', array(&$this, 'profile_update_action') );
            add_action('admin_menu', array(&$this, 'admin_menu') );   
            add_action( 'admin_init', array(&$this,'membership_plan_head') );                                
        }    
        
        function disable_access_adminpanel() {
            get_currentuserinfo();
            global $user_level;
            if( trim($_SERVER['PHP_SELF'], "/" ) != 'wp-admin/admin-ajax.php' ) {
                if ( ( $user_level < 7 ) && is_admin() ) {
                     wp_redirect( home_url() );
                     exit;
                }
            }
        }
        
        function disable_admin_toolbar()
        {
            get_currentuserinfo();
            global $user_level;
            if (( $user_level < 7 ) && !is_admin() ) {
                 show_admin_bar(false);
            }
        }
        
        function redirect_login_to_front_page() {
            global $redirect_to;
            if (!isset($_GET['redirect_to'])) 
            {
                $redirect_to = get_option('siteurl');
            }
        }
        
        function redirect_logout_to_front_page() {
            wp_redirect( home_url() );
            exit();
        }
        
        function create_ace_account() {
            global $GLOBALS;
            $nonce = $_REQUEST['_wpnonce'];
            if ( ( isset($_POST['action']) && $_POST['action'] == 'create_ace_account' ) && ( wp_verify_nonce( $nonce, 'create_ace_account' ) ) ) {
                
                $proceed = true;
                $error = array();
                foreach(array('parentName', 'parentEmail', 'userID', 'password', 'confirmPassword', 'studentName', 'studentUserID', 'studentPassword') as $key) {
                    if( !isset($_POST[$key]) ) {
                        $proceed = false;
                        $error['empty'][] = $key;
                    }
                    if( $_POST[$key] == '') {
                       $proceed = false;
                        $error['empty'][] = $key;                       
                    }
                }
                
                $parentName = $_POST['parentName'];
                $parentEmail = $_POST['parentEmail'];
                $parentUserID = $_POST['userID'];
                $parentPassword = $_POST['password'];
                $parentPassword2 = $_POST['confirmPassword'];
                $studentName = $_POST['studentName'];
                $studentUserID = $_POST['studentUserID'];
                $studentPassword = $_POST['studentPassword'];
                
                if($parentPassword != $parentPassword2) {
                    $proceed = false;
                    $error['password'] = true;
                }
                
                if($parentUserID == $studentUserID) {
                    $proceed = false;
                    $error['userid'] = true;
                }
                
                if( username_exists( $parentUserID ) ) {
                    $proceed = false;
                    $error['parent_username'] = true;
                }
                
                if( email_exists( $parentEmail ) ) {
                    $proceed = false;
                    $error['parent_email'] = true;
                }
                  
                if( username_exists( $studentUserID ) ) {
                    $proceed = false;
                    $error['student_username'] = true;
                } 
                
                if($proceed) {
                    // add parent
                    $parent_id = wp_create_user( $parentUserID, $parentPassword, $parentEmail );
                    $GLOBALS['create_ace_account_parent'] = $parent_id;
                    
                    if( ! isset( $parent_id->errors ) ) {
                         wp_update_user( array(
                            'ID' => $parent_id,
                            'display_name' => $parentName,
                            'nickname' => $parentName,
                            'first_name' => $parentName,
                            'role' => 'parent',
                         ) );
                         add_user_meta( $parent_id, '_paid', 0, true );
                         
                         $student_id = wp_create_user( $studentUserID, $studentPassword, $studentUserID . " . " .$parentEmail );
                         $GLOBALS['create_ace_account_student'] = $student_id;
                         
                         if( ! isset( $student_id->errors ) ) {
                             wp_update_user( array(
                                'ID' => $student_id,
                                'display_name' => $studentName,
                                'nickname' => $studentName,
                                'first_name' => $studentName,
                                'role' => 'student',
                             ) );
                             add_user_meta( $student_id, '_paid', 0, true );
                             add_user_meta( $student_id, '_parent_id', $parent_id, true );
                         }
                    }
                    
                } else { 
                    $GLOBALS['create_ace_account_error'] = $error;
                }
            }
        }
        
        function restrict_direct_access_wplogin() {
           // if( trim($_SERVER['PHP_SELF'], "/" ) == 'wp-login.php' ) {
                if( ( ! isset( $_GET['action'] ) || ( $_GET['action'] != 'logout')) && ( count($_POST) <= 0 ) ) {
                    header("location: " . get_permalink( get_page_by_path( 'login' ) ) );
                    exit;
                }
           // }
        }
        
        function no_access_wplogin() {
            $redirect = true;
            
            if( isset($_GET['action']) && $_GET['action'] == 'logout' ) {
                $redirect = false;
            }
            
            if( $redirect ) {
                    header("location: " . get_permalink( get_page_by_path( 'login' ) ) );
                    exit;
            }
            
        }
        
        function add_ace_student() {
            
            global $current_user, $GLOBALS;
            get_currentuserinfo();
            
            $nonce = $_REQUEST['_wpnonce'];
            if ( 
            ( isset($_POST['action']) && $_POST['action'] == 'add_ace_student' ) 
            && ( wp_verify_nonce( $nonce, 'add_ace_student_' . $current_user->ID ) ) 
            && ( isset($_POST['display_name']) && $_POST['display_name'] != '') 
            && ( isset($_POST['email']) && $_POST['email'] != '')
            && ( isset($_POST['username']) && $_POST['username'] != '')
            && ( isset($_POST['password']) && $_POST['password'] != '')
            ) {
                   
                    $student_id = wp_create_user( $_POST['username'], $_POST['password'], $_POST['email'] );
                    if( is_wp_error( $student_id ) ) {
                        $GLOBALS['add_ace_student_error'] = $student_id->errors;
                    } else {
                    
                     wp_update_user( array(
                                'ID' => $student_id,
                                'display_name' => $_POST['display_name'],
                                'nickname' => $_POST['display_name'],
                                'first_name' => $_POST['display_name'],
                                'role' => 'student',
                             ) );
                             add_user_meta( $student_id, '_paid', 0, true );
                             add_user_meta( $student_id, '_parent_id', $current_user->ID, true );
                             
                            if ( isset($_POST['gradelevel']) && $_POST['gradelevel'] != '') {
                                update_user_meta( $student_id, '_gradelevel', $_POST['gradelevel'] );
                            }
                             
                             header('location: ' . get_permalink( get_page_by_path( 'my-students' ) ) );
                             exit;
                    } 
                    
                
            }
        }
        
        function manage_ace_student() {
            
            global $current_user, $GLOBALS;
            get_currentuserinfo();
            
            $nonce = $_REQUEST['_wpnonce'];
            if ( 
            ( isset($_POST['action']) && $_POST['action'] == 'manage_ace_student' ) 
            && ( wp_verify_nonce( $nonce, 'manage_ace_student_' . $current_user->ID . '_' . $_POST['student_id'] ) ) 
            && ( isset($_POST['display_name']) && $_POST['display_name'] != '') 
            && ( isset($_POST['email']) && $_POST['email'] != '')
            //&& ( isset($_POST['username']) && $_POST['username'] != '')
            && ( isset( $_POST['student_id'] ) && $_POST['student_id'] != '')
            ) {
            
                    $parent_id = get_user_meta( $_POST['student_id'], '_parent_id', true );
                    if( $current_user->ID == $parent_id ) {
                    $userdata = array(
                                'ID' => $_POST['student_id'],
                                'display_name' => $_POST['display_name'],
                                'nickname' => $_POST['nickname'],
                                'first_name' => $_POST['firstname'],
                                'last_name' => $_POST['lastname'],
                                'user_email' => $_POST['email'],
                             );
                             
                            if ( isset( $_POST['newpassword'] ) && $_POST['newpassword'] != '') {
                                $userdata['user_pass'] = $_POST['newpassword'];
                            }
                            
                        wp_update_user( $userdata ); 
                        
                        if ( isset($_POST['gradelevel']) && $_POST['gradelevel'] != '') {
                            update_user_meta( $_POST['student_id'], '_gradelevel', $_POST['gradelevel'] );
                        }
                        
                        if ( isset($_POST['email-alerts']) && $_POST['email-alerts'] != '') {
                            update_user_meta( $_POST['student_id'], '_emailalerts', true );
                        } else {
                            update_user_meta( $_POST['student_id'], '_emailalerts', false );
                        }
                        
                        $GLOBALS['manage_student_updated'] = true;        
                    } 
                    
                
            }
        }
        
        function delete_ace_student() {
            
            global $current_user;
            get_currentuserinfo();
            
            $nonce = $_REQUEST['_wpnonce'];
            if ( 
            ( isset($_POST['action']) && $_POST['action'] == 'delete_ace_student' ) 
            && ( wp_verify_nonce( $nonce, 'delete_ace_student_' . $current_user->ID . '_' . $_POST['student_id'] ) ) 
            && ( isset($_POST['student_id']) && $_POST['student_id'] != '') 
            ) {
                    $paid = get_user_meta( $_POST['student_id'], '_paid', true );
                    $parent_id = get_user_meta( $_POST['student_id'], '_parent_id', true );
                    if( $current_user->ID == $parent_id && $paid == 0) {
                        require_once(ABSPATH.'wp-admin/includes/user.php' );
                        wp_delete_user( $_POST['student_id'] );
                        header('location: ' . get_permalink( get_page_by_path( 'my-students' ) ) );
                        exit;
                    }
                    
                
            }
        }
        
        function activate_ace_student() {
            $nonce = $_REQUEST['_wpnonce'];
            if ( ( isset($_POST['action']) && $_POST['action'] == 'activate_ace_student' ) 
            && ( wp_verify_nonce( $nonce, 'activate_ace_student_' . $_POST['parent_id'] . '_' . $_POST['student_id'] ) )
            && ( isset($_POST['parent_id']) && $_POST['parent_id'] != '') 
            && ( isset($_POST['student_id']) && $_POST['student_id'] != '')  
            ) {
           
               $parent_id = get_user_meta( $_POST['student_id'], '_parent_id', true );
                if( $_POST['parent_id'] == $parent_id ) {
                    update_user_meta( $_POST['student_id'], '_paid', 1 );
                    header('location: ' . get_permalink( get_page_by_path( 'my-students' ) ) );
                    exit;
                }

            }
        }
        
        function pay_ace_accounts() {
            $nonce = $_REQUEST['_wpnonce'];
            if ( ( isset($_POST['action']) && $_POST['action'] == 'pay_ace_accounts' ) 
            && ( wp_verify_nonce( $nonce, 'pay_ace_accounts_' . $_POST['parent_id'] . '_' . $_POST['student_id'] ) )
            && ( isset($_POST['parent_id']) && $_POST['parent_id'] != '') 
            && ( isset($_POST['student_id']) && $_POST['student_id'] != '')  
            ) {
           
               $parent_id = get_user_meta( $_POST['student_id'], '_parent_id', true );
                if( $_POST['parent_id'] == $parent_id ) {
                    update_user_meta( $_POST['parent_id'], '_paid', 1 );
                    update_user_meta( $_POST['student_id'], '_paid', 1 );
                    $membership_plan = ( $_POST['membership_plan'] ) ? $_POST['membership_plan'] : get_option('_ace_membership_plan_default');
                    update_user_meta( $_POST['parent_id'], '_membership_plan', $membership_plan ); 
                    update_user_meta( $_POST['student_id'], '_membership_plan', $membership_plan ); 
                }

            }
        }
        
        function renew_ace_account() {
            $nonce = $_REQUEST['_wpnonce'];
            
            if ( 
                ( isset($_POST['parent_id']) && $_POST['parent_id'] != '')
                && ( isset($_POST['student_id']) && $_POST['student_id'] != '')  
            && ( ( isset($_POST['action']) && $_POST['action'] == 'renew_ace_account' ) && ( wp_verify_nonce( $nonce, 'renew_ace_account_' . $_POST['parent_id'] . '_' . $_POST['student_id']  ) ) )
            ) {
                    $old_expiry = get_user_meta( $_POST['student_id'] , '_expiry', true);
                    if ( $old_expiry != '' && ( strtotime( $old_expiry ) > strtotime( date( "Y-m-d" ) ) ) ) {
                        $new_expiry = date("Y-m-d", strtotime("+1 month +1 day", strtotime(  $old_expiry  )));
                    } else {
                        $new_expiry = date("Y-m-d", strtotime("+1 month +1 day", strtotime( date( "Y-m-d" ) )));
                    }
                    update_user_meta( $_POST['parent_id'], '_expiry', $new_expiry, 0 );
                    update_user_meta( $_POST['student_id'], '_expiry', $new_expiry, 0 );
                
            }
        }
        
        function upgrade_ace_student() {
            global $current_user;
            $nonce = $_REQUEST['_wpnonce'];
            
            if ( 
                ( isset($_POST['plan_id']) && $_POST['plan_id'] != '')
                && ( isset($_POST['student_id']) && $_POST['student_id'] != '')  
            && ( ( isset($_POST['action']) && $_POST['action'] == 'upgrade_ace_student' ) && ( wp_verify_nonce( $nonce, 'upgrade_ace_student_' . $current_user->ID . '_' . $_POST['student_id']  ) ) )
            ) {
                    update_user_meta( $_POST['student_id'], '_membership_plan', $_POST['plan_id'] );
                
            }
        }
        
        function login_ace_account() {
            $nonce = $_REQUEST['_wpnonce'];
            $login_url = get_permalink( get_page_by_path( 'login' ) );
            
            if ( ( isset($_POST['action']) && $_POST['action'] == 'login_ace_account' ) && ( wp_verify_nonce( $nonce, 'login_ace_account' ) ) ) {   
                $creds = array();
                $creds['user_login'] = $_POST['username'];
                $creds['user_password'] = $_POST['password'];
                $creds['remember'] = true;
                
                if( username_exists( $_POST['username'] ) ) { 
                    $userdata =  get_user_by('login', $_POST['username']) ;
                    if ( $userdata ) {
                        $role = array_shift($userdata->roles);
                        if( ( $role == 'administrator') || ( $role == 'editor') ) {
                            $user = wp_signon( $creds, false );
                        } elseif( ( $role == 'parent') || ($role == 'student')) {
                            $paid = get_user_meta( $userdata->ID , '_paid', true );
                            
                            if($paid) { 
                                   $user = wp_signon( $creds, false );
                            } else {
                                    
                                   if( strpos($login_url , '?') > 0 ) {
                                        $login_url .= '&notactivated=true';
                                    } else {
                                        $login_url .= '?notactivated=true';
                                    }
                                    
                                        header("location: " . $login_url );
                                        exit;  
                                }
                            
                        }
                    }
                    
                    if ( is_wp_error( $user ) ) {
                        //$user->get_error_message(); 
           
                        if( strpos($login_url , '?') > 0 ) {
                            $login_url .= '&error=true';
                        } else {
                            $login_url .= '?error=true';
                        }
                        
                            header("location: " . $login_url );
                            exit;  
                         
                    } else {
                        $redirect = (isset($_POST['redirect_to'])) ? $_POST['redirect_to'] : get_bloginfo('home');
                        header('location: ' . $redirect );
                        exit;
                    }

                } else {
                    if( strpos($login_url , '?') > 0 ) {
                            $login_url .= '&error=true';
                        } else {
                            $login_url .= '?error=true';
                        }
                        
                            header("location: " . $login_url );
                            exit;
                }
            }  
        }
        
        function update_my_account() {
            $nonce = $_REQUEST['_wpnonce'];
            $user_id = $_REQUEST['user_id'];
  
            if ( ( isset($_POST['action']) && $_POST['action'] == 'update_my_account' ) && ( wp_verify_nonce( $nonce, 'update_my_account_' . $user_id ) ) ) {
                $userdata = array(
                                'ID' => $user_id,
                                'display_name' => $_REQUEST['displayname'],
                                'nickname' => $_REQUEST['nickname'],
                                'first_name' => $_REQUEST['firstname'],
                                'last_name' => $_REQUEST['lastname'],                                
                             );
                             

                             if( ( $_REQUEST['newpassword'] != '') && ( $_REQUEST['newpassword'] == $_REQUEST['confirmpassword'] ) ) {
                                $userdata['user_pass'] = $_REQUEST['newpassword'];
                                $GLOBALS['my_account_password_update'] = true;  
                             }
                             
                wp_update_user(  $userdata ); 
                $GLOBALS['my_account_update'] = true;    
                                       
            }
        }

        function profile_edit_action($user) {
           $checked = ( get_user_meta($user->ID, '_paid', true) )  ? ' checked="checked"' : '';
        ?>
          <h3>Payment Status</h3>
          <label for="_paid">
            <input name="_paid" type="checkbox" id="_paid" value="1"<?php echo $checked; ?>>
            Paid
          </label>
        <?php 
        }
        
        function profile_update_action($user_id) {
            update_user_meta($user_id, '_paid', isset($_POST['_paid']) );
        }
        
        function admin_menu() {
            add_submenu_page( 'acemymath', 'Membership Levels', 'Membership', 'manage_options', 'acemymath-membership', array(&$this, 'admin_page') ); 
        }
        
        function membership_plan_head() {
            if( isset( $_POST['action']) && ($_POST['action'] == 'guest_restrictions')  ) {
                if( isset( $_POST['levels'] ) ) {
                    update_option('_ace_guest_restriction_levels', serialize($_POST['levels']));
                 } else {
                    delete_option('_ace_guest_restriction_levels');
                 }
                 if( isset( $_POST['lessons'] ) ) {
                    update_option('_ace_guest_restriction_lessons', serialize($_POST['lessons']));
                 } else {
                    delete_option('_ace_guest_restriction_lessons');
                 }
            }
            if( isset( $_POST['action']) && ($_POST['action'] == 'add_new_plan') && isset( $_POST['plan-name']) && ($_POST['plan-name'] != '') ) {
                $newID = $this->add_membership_plan( $_POST['plan-name'], $_POST['plan-level'], $_POST['description'] , serialize($_POST['levels']), serialize($_POST['lessons']), serialize($_POST['problems']), serialize($_POST['worksheets']) );
                header("location: admin.php?page=acemymath-membership&action=edit&id=" . $newID . "&msg=1");
                exit;
            }
            
           if( isset( $_POST['action']) && ($_POST['action'] == 'update_plan') && isset( $_POST['plan-id']) && ($_POST['plan-id'] != '') && isset( $_POST['plan-name']) && ($_POST['plan-name'] != '') ) {                
                $this->update_membership_plan( $_POST['plan-id'], $_POST['plan-name'], $_POST['plan-level'], $_POST['description'] , serialize($_POST['levels']), serialize($_POST['lessons']), serialize($_POST['problems']), serialize($_POST['worksheets']) );
                header("location: admin.php?page=acemymath-membership&action=edit&id=" . $_POST['plan-id'] . "&msg=3");
                exit;
            }
            
           if( isset( $_GET['action']) && ($_GET['action'] == 'delete') && isset( $_GET['id']) && ($_GET['id'] != '') ) {
                if( wp_verify_nonce( $_REQUEST['_wpnonce'], 'delete_membership_plan_'.$_REQUEST['id'] ) ) {
                    $this->delete_membership_plan( $_REQUEST['id'] );
                }
                header("location: admin.php?page=acemymath-membership&msg=2");
                exit;
           }
           if( isset( $_GET['action']) && ($_GET['action'] == 'default') && isset( $_GET['id']) && ($_GET['id'] != '') ) {
                if( wp_verify_nonce( $_REQUEST['_wpnonce'], 'default_membership_plan_'.$_REQUEST['id'] ) ) {
                    update_option('_ace_membership_plan_default', $_REQUEST['id'] );
                }
                header("location: admin.php?page=acemymath-membership&msg=4");
                exit;
           }

        }
        
        function admin_page() {

$default_plan = get_option('_ace_membership_plan_default');

$action = ( isset( $_GET['action'] ) ) ? $_GET['action'] : NULL;

            echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
                echo '<h2>Membership Plans';
                if( ! $action ) {
                    echo '<a href="admin.php?page=acemymath-membership&action=add" class="add-new-h2">Add New Plan</a>';                    
                }
                echo '</h2>';

if( isset($_GET['msg']) ) {
switch ($_GET['msg'] ) {
    case '1':
        echo '<div id="message" class="updated below-h2"><p>Plan added!</p></div>';
    break;
    case '2':
        echo '<div id="message" class="updated below-h2"><p>Plan deleted.</p></div>';
    break;
    case '3':
        echo '<div id="message" class="updated below-h2"><p>Plan updated.</p></div>';
    break;
    case '4':
        echo '<div id="message" class="updated below-h2"><p>Set default plan.</p></div>';
    break;
}
}


switch( $action ) {
default:

echo <<<TABLE
<table class="wp-list-table widefat fixed tags" cellspacing="0">
    <thead>
    <tr>
        <th scope="col" id="name" class="manage-column column-name sortable desc" style="">
            <a>
                <span>Name</span>
</a>
        </th>
        <th scope="col" id="description" class="manage-column column-description sortable desc" style="">
                <span>Description</span>
        </th>

                <th scope="col" id="slug" class="manage-column column-slug sortable desc" style="">
                <span>Level</span>
        </th>
        <th scope="col" id="posts" class="manage-column column-posts num sortable desc" style="">
                <span>Subscriptions</span>
        </th>    
    </tr>
    </thead>
    <tbody id="the-list">
TABLE;

$n=0;
$tobeplanned = '';

foreach( $this->get_membership_plan() as $plan ) {

$tobeplanned = $plan->plan_id;
$delete_url = wp_nonce_url( 'admin.php?page=acemymath-membership&action=delete&id=' . $plan->plan_id  , 'delete_membership_plan_' . $plan->plan_id );
$default_url = wp_nonce_url( 'admin.php?page=acemymath-membership&action=default&id=' . $plan->plan_id  , 'default_membership_plan_' . $plan->plan_id );
$alternate = ($n % 2) ? '' : 'alternate';

echo <<<TABLE
        <tr id="tag-9" class="{$alternate}">
            <td class="name column-name">
            <strong>
                <a class="row-title" href="admin.php?page=acemymath-membership&action=edit&id={$plan->plan_id}" title="Edit “{$plan->plan_name}”">
                {$plan->plan_name}
                </a>
            </strong>
            <br>
            <div class="row-actions">
                <span class="edit">
                    <a href="admin.php?page=acemymath-membership&action=edit&id={$plan->plan_id}" title="Edit “{$plan->plan_name}”">Edit</a> | 
                </span>
                
TABLE;
if  ($default_plan == $plan->plan_id)  {
    echo '<span class="delete"><a>Default Plan</a></span>';
} else {
    
    echo '<span class="default"><a class="default-plan" href="'.$default_url.'">Make Default</a></span> | <span class="delete"><a class="delete-plan" href="'.$delete_url.'">Delete</a></span>';
}
echo <<<TABLE
                
            </div>
            </td>
            <td class="description column-description">{$plan->description}</td>
            <td class="slug column-slug">{$plan->plan_level}</td>
            <td class="posts column-posts">
            <a href="admin.php?page=acemymath-membership&action=subscribers&plan_id={$plan->plan_id}">
            0
            </a>
            </td>
        </tr>    
TABLE;
$n++;

}

echo <<<TABLE
    </tbody>
</table>
<p class="submit"><a href="admin.php?page=acemymath-membership&action=guest" class="button button-primary">Guest Restrictions</a></p></form></div>
TABLE;
break;

case 'guest':

$guest_restriction_levels =  get_option('_ace_guest_restriction_levels') ;
$guest_restriction_lessons =  get_option('_ace_guest_restriction_lessons') ;

echo <<<HTML
<hr>
<h2>Guest Restrictions</h2>
<form action="" method="POST">
<input type="hidden" name="action" value="guest_restrictions" />
HTML;
$this->membership_plan_restrictions(true, $guest_restriction_levels, $guest_restriction_lessons );
echo <<<HTML
    <p class="submit"><input type="submit" id="submit" class="button button-primary" value="Save"></p></form>
HTML;
break;

case 'add' :
echo <<<HTML
<div id="col-container">

<form id="addtag" method="post" action="admin.php?page=acemymath-membership" class="validate">
<div id="col-right">
<div class="col-wrap">
HTML;
$this->membership_plan_restrictions();
echo <<<HTML
</div>
</div><!-- /col-right -->

<div id="col-left">
<div class="col-wrap">

<div class="form-wrap">
<h3>Add New Membership Plan</h3>
<input type="hidden" name="action" value="add_new_plan" />
<div class="form-field form-required">
    <label for="tag-name">Plan Name</label>
    <input name="plan-name" id="plan-name" type="text" value="" size="40" aria-required="true">
</div>

<div class="form-field">
    <label for="tag-description">Description</label>
    <textarea name="description" id="tag-description" rows="5" cols="40"></textarea>
</div>

<div class="form-field form-required">
    <label for="tag-name">Plan Level</label>
    <input name="plan-level" id="plan-level" type="text" value="" size="5" aria-required="true">
</div>

<p class="submit"><input type="submit" id="submit" class="button button-primary" value="Add New Plan"></p></form></div>

</div>
</div><!-- /col-left -->

</div>
HTML;
break;

case 'edit' :

$current_plan = ( isset( $_GET['id'] ) && $_GET['id'] != '') ? $this->get_membership_plan( $_GET['id'] ) : false;

if( $current_plan && isset($current_plan[0]) ) :

$plan = $current_plan[0];

echo <<<HTML
<form id="addtag" method="post" action="admin.php?page=acemymath-membership" class="validate">
<div id="col-container">

<div id="col-right">
<div class="col-wrap">
HTML;
$this->membership_plan_restrictions( true, $plan->levels, $plan->lessons );
echo <<<HTML
</div>
</div><!-- /col-right -->

<div id="col-left">
<div class="col-wrap">

<div class="form-wrap">
<h3>Add New Membership Plan</h3>
<input type="hidden" name="action" value="update_plan" />
<input type="hidden" name="plan-id" value="{$plan->plan_id}" />
<div class="form-field form-required">
    <label for="tag-name">Plan Name</label>
    <input name="plan-name" id="plan-name" type="text" value="{$plan->plan_name}" size="40" aria-required="true">
</div>
<div class="form-field">
    <label for="tag-description">Description</label>
    <textarea name="description" id="tag-description" rows="5" cols="40">{$plan->description}</textarea>
</div>

<div class="form-field form-required">
    <label for="tag-name">Plan Level</label>
    <input name="plan-level" id="plan-level" type="text" value="{$plan->plan_level}" size="5" aria-required="true">
    <br><small>This will determine if parent account needs to be upgraded.</small>
</div>

<p class="submit"><input type="submit" id="submit" class="button button-primary" value="Save Changes"></p></div>

</div>
</div><!-- /col-left -->



</div>
</form>
HTML;

endif;

break;
}


            echo '</div>';
        }
        
        
function membership_plan_restrictions($edit=false, $planlevels=NULL, $planlessons=NULL, $planproblems=NULL, $planworksheets=NULL) {

$plan_levels = unserialize( $planlevels );
$plan_lessons = unserialize( $planlessons );
$plan_problems = unserialize( $planproblems );
$plan_worksheets = unserialize( $planworksheets );

// levels 
$levels_raw = get_terms('level', 'hide_empty=0');
$levels = array();

foreach($levels_raw as $lvlraw) {   
    $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
    if( isset( $levels[$order]) ) {
        $levels[] = $lvlraw;
    } else {
        $levels[$order] = $lvlraw;
    }
}

ksort($levels);

echo <<<HTML
<div class="accordion" id="membership-plans-restrictions-accordion">
 <h3>Grade Level Restrictions</h3>
  <div>
    

<table class="wp-list-table widefat fixed tags" cellspacing="0">
    <!--<thead>
    <tr>
        <th scope="col" id="cb" class="manage-column column-cb check-column" style="">
        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
        <input id="cb-select-all-1" checked type="checkbox"></th>
        <th scope="col" id="name" class="manage-column column-name sortable desc" style=""><a><span>Grade Level</span></a></th>
    </tr>
    </thead>-->
    <tbody id="the-list">
HTML;

$n=0;
foreach($levels as $level) {
$alternate = ($n % 2) ? '' : 'alternate';
$level_checked = '';
if( $edit ) {
   $level_checked = ( is_array( $plan_levels ) && array_search($level->term_id, $plan_levels) !== FALSE ) ? 'checked' : ''; 
}
echo <<<HTML
            <tr id="tag-9" class="{$alternate}">
                <th scope="row" class="check-column">
                    <label class="screen-reader-text" for="cb-select-9">Select {$level->name}</label>
                    <input class="level-checkbox" type="checkbox" {$level_checked} name="levels[]" value="{$level->term_id}" id="cb-select-{$level->term_id}">
                </th>
                <td class="name column-name"><strong>{$level->name}</strong></td>
            </tr>
HTML;
$n++;
}

echo <<<HTML
            </tbody>
    </table>
    
  </div>
  <h3>Lesson Restrictions</h3>
  <div>


<p>
    <label for="parent">Lessons Access Duration</label>
<select name="lessons[hours]" id="parent" class="postform">
HTML;

for($i=0; $i <= 24; $i++) {
    $selected = ( $edit && isset($plan_lessons['hours']) && $plan_lessons['hours'] == $i) ? 'SELECTED' : '';
    echo '<option value="'.$i.'" '.$selected.'>'. $i . ' Hour(s)</option>';
}

echo <<<HTML
</select>

<select name="lessons[minutes]" id="parent" class="postform">
HTML;

for($i=0; $i <= 59; $i++) {
    $selected = ( $edit && isset($plan_lessons['minutes']) && $plan_lessons['minutes'] == $i) ? 'SELECTED' : '';
    echo '<option value="'.$i.'" '.$selected.'>'. $i . ' Minute(s)</option>';
}

echo <<<HTML
</select>

</p>

<div id="level-tabs">
  <ul>
HTML;

$n=0;
foreach($levels as $level) {


echo "<li id=\"lesson-level-nav-{$level->term_id}\"><a href=\"#lesson-level-tab-{$level->term_id}\">{$level->name}</a></li>\n";
$n++;
}

echo '</ul>';

$n=0;
foreach($levels as $level) {
$alternate = ($n % 2) ? '' : 'alternate';


echo "<div id=\"lesson-level-tab-{$level->term_id}\">
  <div class=\"accordion\" id=\"lessons-chapters-accordion\">
";

$chapters_raw = get_terms('chapter');
$chapters = array();
foreach($chapters_raw as $chptraw) {   
    $order = (int) get_custom_termmeta($chptraw->term_id, 'menu_order', true);
    if( isset( $chapters[$order] ) ) {
        $chapters[] = $chptraw;
    } else {
        $chapters[$order] = $chptraw;
    }
}
ksort($chapters); 

$c = 0;
if( count($chapters) > 0 ) :
    foreach($chapters as $chapter) :
    
    $chapter_title =  get_custom_termmeta($chapter->term_id, 'chapter_title_' . $level->term_id, true);
    
    $lessons = get_ace_lessons($level->term_id, $chapter->term_id) ;

    if( $lessons ) :
    
    echo <<<HTML
  
    <h3>{$chapter->name} : {$chapter_title}</h3>
    <div>
<table class="wp-list-table widefat fixed tags" cellspacing="0">
    <!--<thead>
    <tr>
        <th scope="col" id="cb" class="manage-column column-cb check-column" style="">
        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
        <input id="cb-select-all-1" type="checkbox" class="lesson-checkbox-level-{$level->term_id}"></th>
        <th scope="col" id="name" class="manage-column column-name sortable desc" style=""><a><span>{$chapter->name} : {$chapter_title}</span></a></th>
    </tr>
    </thead>-->
    <tbody id="the-list">
HTML;
    $l=0;
    foreach( $lessons as $lesson ) { 
        $alternate = ($l % 2) ? '' : 'alternate';
        
   $lesson_checked = ( $edit && is_array( $plan_lessons['restrictions'] ) && array_search($lesson->ID, $plan_lessons['restrictions']) !== FALSE ) ? 'CHECKED' : ''; 

    
echo <<<HTML
            <tr id="tag-9" class="{$alternate}">
                <th scope="row" class="check-column">
                    <label class="screen-reader-text" for="cb-select-9">Select asdf asdf asdf</label>
                    <input class="lesson-checkbox lesson-checkbox-level-{$level->term_id} lesson-checkbox-chapter-{$chapter->term_id}" type="checkbox" {$lesson_checked} name="lessons[restrictions][]" value="{$lesson->ID}" id="cb-select-{$lesson->ID}">
                </th>
                <td class="name column-name"><strong>Lesson {$lesson->menu_order}: {$lesson->post_title}</strong></td>
            </tr>
HTML;
$l++;
    }
echo <<<HTML
            </tbody>
    </table>
    </div>
HTML;
    
    endif;
$c++;
    endforeach;
endif;

echo "  </div></div>\n";

$n++;
}

echo <<<HTML
  
</div>

  </div>
  <!--
  <h3>Problems Restrictions</h3>
  <div>

  </div>
  <h3>Worksheets Restrictions</h3>
  <div>
    
  </div>
  -->
</div>
HTML;

        }


        function add_membership_plan($name, $plan_level=0, $description='', $levels='', $lessons='', $problems='', $worksheets='') {
            global $wpdb;
            $sql = "INSERT INTO `{$wpdb->prefix}ace_membership_plans` (`plan_id`, `plan_name`, `plan_level`, `description`, `levels`, `lessons`, `problems`, `worksheets`) VALUES (NULL, '{$name}', '{$plan_level}',  '{$description}', '{$levels}', '{$lessons}', '{$problems}', '{$worksheets}');";
            
            $wpdb->query( $sql );
            return $wpdb->insert_id;
        }   
        
        function update_membership_plan($id, $name, $plan_level=0, $description='', $levels='', $lessons='', $problems='', $worksheets='') {
            global $wpdb;
            $sql = "UPDATE `{$wpdb->prefix}ace_membership_plans` SET  `plan_name` = '{$name}', 
            `plan_level` = '{$plan_level}',
            `description` =  '{$description}',
`levels` =  '{$levels}',
`lessons` =  '{$lessons}',
`problems` =  '{$problems}',
`worksheets` =  '{$worksheets}' 
WHERE `plan_id` = {$id};
";
            
            $wpdb->query( $sql );
        } 

        function delete_membership_plan($id) {
            global $wpdb;
            $sql = "DELETE FROM `{$wpdb->prefix}ace_membership_plans` WHERE `{$wpdb->prefix}ace_membership_plans`.`plan_id` = " . $id;
            return $wpdb->query( $sql );
        } 
                
         function get_membership_plan($id=NULL) {
            global $wpdb;
            $sql = "SELECT * FROM  `wp_ace_membership_plans` ";
                if( $id != NULL ) {
                    $sql .= " WHERE `plan_id` = " . $id;
                }
            $sql .= " ORDER BY `plan_level` ASC, `plan_name` ASC";
            return $wpdb->get_results( $sql );
        }  
        
        function add_membership_session() {
            if ( ! isset( $_COOKIE['acemymath_guest_session'] ) && ! is_user_logged_in() ) {
            
                global $wpdb;
                $sql = "SELECT * FROM  `{$wpdb->prefix}ace_sessions` WHERE `ip` = '" . $_SERVER['REMOTE_ADDR'] . "';";
                $session = $wpdb->get_results( $sql );
                
                if( isset( $session[0] ) ) {
                    $now = strtotime( date("Y-m-d") );
                    $yesterday = strtotime( date( 'Y-m-d', mktime( $session[0]->started ) ) );
                    if( $now != $yesterday ) {
                        $sql = "UPDATE  `{$wpdb->prefix}ace_sessions` SET  `started` =  CURRENT_TIMESTAMP() WHERE `id` = {$session[0]->id}";
                        $wpdb->query( $sql );
                        setcookie('acemymath_guest_session', $session[0]->id, time()+3600*24*100, COOKIEPATH, COOKIE_DOMAIN, false);
                    }
                } else {
                    $sql = "INSERT INTO  `{$wpdb->prefix}ace_sessions` (`id` ,`ip`) VALUES ( NULL ,  '" . $_SERVER['REMOTE_ADDR'] . "' );";
                    $wpdb->query( $sql );
                    setcookie('acemymath_guest_session', $wpdb->insert_id, time()+3600*24*100, COOKIEPATH, COOKIE_DOMAIN, false);
                }
                
            }
            
        } 
        
    }

}

function AceSubscriptionPlans($id=NULL) {
        global $wpdb;
            $sql = "SELECT * FROM  `{$wpdb->prefix}ace_membership_plans` ";
                if( $id != NULL ) {
                    $sql .= " WHERE `plan_id` = " . $id;
                }
            $sql .= " ORDER BY `plan_level` ASC, `plan_name` ASC";
            $results = $wpdb->get_results( $sql );
            if( $id != NULL ) {
                return (isset($results[0])) ? $results[0] : $results;
            } else {
                return $results;
            }
}

function AceCurrentUserLevelsAllowed() {
    global $current_user;
    get_currentuserinfo();
    $user_plan_id = ( get_user_meta( $current_user->ID, '_membership_plan', true) ) ? get_user_meta( $current_user->ID, '_membership_plan', true) : get_option('_ace_membership_plan_default');
    $user_plan = AceSubscriptionPlans( $user_plan_id );
    $rules = ( isset( $user_plan->levels ) ) ? unserialize( $user_plan->levels ) : array();
    return $rules;
}

function AceCurrentUserLessonsAllowed() {
    global $current_user;
    get_currentuserinfo();
    $user_plan_id = ( get_user_meta( $current_user->ID, '_membership_plan', true) ) ? get_user_meta( $current_user->ID, '_membership_plan', true) : get_option('_ace_membership_plan_default');
    $user_plan = AceSubscriptionPlans( $user_plan_id );
    $rules = ( isset( $user_plan->lessons ) ) ? unserialize( $user_plan->lessons ) : array();
    $allowed = ( isset( $rules['restrictions'] ) ) ? $rules['restrictions'] : array();
    return $allowed;
}

function AceGuestLevelsAllowed() {
    return unserialize( get_option('_ace_guest_restriction_levels') );
}

function AceGuestLessonsAllowed() {
    $restrictions = unserialize( get_option('_ace_guest_restriction_lessons') );
    return ( isset( $restrictions['restrictions'] ) ) ? $restrictions['restrictions'] : array();
}

function AceMembershipSession() {
        global $wpdb;
        $sql = "SELECT * FROM  `{$wpdb->prefix}ace_sessions` WHERE `id` = " . $_COOKIE['acemymath_guest_session'];
        return $wpdb->get_results( $sql );
}



