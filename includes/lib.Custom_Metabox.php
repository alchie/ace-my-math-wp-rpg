<?php
/* 
Name: Custom Post Type Class 
Filename: Custom_Post_Type.php
Author: Chester Alan Tagudin
Author URL: http://www.chesteralan.com/
*/

if (! class_exists( 'Custom_Metabox' )) {
    class Custom_Metabox
    {
        var $id;
        var $title;
        var $post_type;
        var $context;
        var $priority;
        var $fields = array();
        var $tabs = array();
        
        function __construct($post_type, $id, $title, $context, $priority) {
            $this->id = $id;
            $this->title = $title;
            $this->post_type = $post_type;
            $this->context = $context;
            $this->priority = $priority;
            return $this;
        }    
        
        function init()
        {
            add_action('add_meta_boxes', array(&$this,'add_meta_box'));
            add_action('save_post', array(&$this, 'save_post'));
        }
        
        function add_meta_box() {
                add_meta_box($this->id, $this->title, array(&$this, 'meta_box_content'), $this->post_type, $this->context, $this->priority);
        }
        
        function meta_box_content()
        {
            global $post;
            echo '<input type="hidden" name="custom_meta_box_nonce_'.$post->ID.'" value="'.wp_create_nonce( 'custom_meta_box_nonce_'. $post->ID ).'" />';
            
            if( count($this->tabs) > 0 ) {
                echo '<div class="metabox-tabs"><ul>';
                    foreach($this->tabs as $tab) {
                        echo '<li '.$default.'><a href="#'.$tab['id'].'">'.$tab['label'].'</a></li>';
                    }
                echo '</ul>';
                
                foreach($this->tabs as $tab) {
                    echo '<div id="'.$tab['id'].'">';
                        if( count($this->fields) > 0) {
                            foreach($this->fields as $field) {
                                if( isset( $field['tab'] ) && $field['tab'] == $tab['id'] ) {
                                    $this->show_field($field, $post->ID);
                                }
                            }
                        }
                    echo '</div>';                    
                }
                
                echo '</div>';
            } else {
                if( count($this->fields) > 0) {
                    foreach($this->fields as $field) {
                        $this->show_field($field, $post->ID);
                    }
                }
            }
       
        }
        
        function add_tab( Array $tab ) {
        
            $defaults = array(
                'id' => '',
                'label' => ''
            );
            
            $this->tabs[] = array_merge($defaults, $tab);
        }
        
        function add_field(Array $field) {
            $defaults = array(
                'id' => '',
                'label' => '',
                'desc' => '',
                'options' => array(),
                'default' => ''
            );
            $this->fields[] =  array_merge($defaults, $field );
        }
        
        function show_field(Array $field, $id) {
            $current_value = get_post_meta($id, $field['id'], true);
            echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
            switch($field['type']) {                    
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="30" />
								<span class="description">'.$field['desc'].'</span>';
					break;
					// textarea
					case 'textarea':
						echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$current_value.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$current_value ? ' checked="checked"' : '','/>
								<label for="'.$field['id'].'">'.$field['desc'].'</label>';
					break;
					// select
					case 'select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';

						foreach ($field['options'] as $option) {
							echo '<option', $current_value == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$current_value == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"',$current_value && in_array($option['value'], $current_value) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['id'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['id']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['id']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
					    $items = get_posts( array (
						    'post_type'	=> $field['post_type'],
						    'posts_per_page' => -1
					    ));
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$current_value == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'datetime':
						echo '<input type="text" class="datetimepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
					$value = $current_value != '' ? $current_value : '0';
						echo '<div id="'.$field['id'].'-slider"></div>
								<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" size="5" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						$image = get_template_directory_uri().'/images/image.png';	
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($current_value) { $image = wp_get_attachment_image_src($current_value, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$current_value.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// repeatable
					case 'repeatable':
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($current_value) {
							foreach($current_value as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="'.$row.'" size="30" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="" size="30" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
					/*
					case 'video-lesson-quiz' :
					    $this->video_lesson_quiz( $id, $field['id'], $current_value );
					break;
					*/
				} //end switch
				echo '</p>';
        }
        
        function save_post( $post_id ) {
	        global $post;

	        // verify nonce
	        if ( ! wp_verify_nonce( $_POST[ 'custom_meta_box_nonce_'. $post_id ], 'custom_meta_box_nonce_'. $post_id ) ) 
		        return $post_id;
		 
	        // check autosave
	        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		        return $post_id;
		
	        // check permissions
	        if ('page' == $_POST['post_type']) {
		        if (!current_user_can('edit_page', $post_id)) {
			        return $post_id;
		        } elseif (!current_user_can('edit_post', $post_id)) {
			        return $post_id; 
			        }
	        }
	
	

		        if( $this->post_type == $post->post_type ) {
			

			        foreach ($this->fields as $field) {
				        if($field['type'] == 'tax_select') continue;

				        $old = get_post_meta($post_id, $field['id'], true);
				
				        $new = $_POST[$field['id']];

				        if ($new && $new != $old) {
					        update_post_meta($post_id, $field['id'], $new);
				        } elseif ('' == $new && $old) {
					        delete_post_meta($post_id, $field['id'], $old);
				        }
			        }
		        }
	        

	         // enf foreach
	
	        // save taxonomies
	        //$post = get_post($post_id);
	        //$category = $_POST['category'];
	        //wp_set_object_terms( $post_id, $category, 'category' );
        }        
        
        /* 
        function video_lesson_quiz($post_id, $field_id, $current_value) {
           $count_index = count( $current_value );
echo <<<HTML
 <ul id="{$this->id}-sortable">
HTML;
$index = 0;
if( ($current_value) && count( $current_value ) > 0) {
foreach($current_value as $quiz ) {

$input_type_choices = ($quiz['input_type'] == 'choices') ? 'CHECKED' : '';
$input_type_choices_display = ($quiz['input_type'] == 'choices') ? 'block' : 'none';

$input_type_input = ($quiz['input_type'] == 'input') ? 'CHECKED' : '';
$input_type_input_display = ($quiz['input_type'] == 'input') ? 'block' : 'none';

echo <<<HTML
      <li id="video-quiz-{$index}" class="">
            <div class="quiz-item-bar">
                <div class="quiz-item-handle">
                    <strong><span id="popupTime-{$index}">{$quiz['popupTime']}</span> - <span id="popupTitle-{$index}">{$quiz['popupTitle']} <small>({$quiz['input_type']})</small></span></strong> 
                    <div class="item-actions">
                        <a href="javascript:void(0);" class="item-edit quiz-item-edit" data-id="{$index}"><div class="dashicons dashicons-edit"></div></a>
                        <a href="javascript:void(0);" class="item-delete quiz-item-delete" data-id="{$index}"><div class="dashicons dashicons-no-alt"></div></a>
                    </div>
                </div>
            </div>
            <div id="quiz-item-settings-{$index}" class="quiz-item-settings">
                 <p>
                    <label>Popup Time (MM:SS)<br>
                        <input type="text" name="{$field_id}[{$index}][popupTime]" value="{$quiz['popupTime']}" class="block input-popup-time" id="input-popup-time-{$index}" data-id="{$index}">
                    </label>
                </p>            
                <p>
                    <label>Question<br>
                        <input type="text" name="{$field_id}[{$index}][popupTitle]" value="{$quiz['popupTitle']}" class="block input-popup-title" id="input-popup-title-{$index}" data-id="{$index}">
                    </label>
                </p>
                <div class="choice">
                <h3>
                <label><input {$input_type_choices} class="choice-type choice-type-{$index}" data-class="{$field_id}-{$index}-choice-type-box" data-id="{$field_id}-{$index}-choice" type="radio" value="choices" name="{$field_id}[{$index}][input_type]"> Multiple Choice</label>
                <label><input {$input_type_input} class="choice-type choice-type-{$index}" data-class="{$field_id}-{$index}-choice-type-box" data-id="{$field_id}-{$index}-input" type="radio" value="input" name="{$field_id}[{$index}][input_type]"> Input Box</label></h3>
<div id="{$field_id}-{$index}-choice" style="display:{$input_type_choices_display}" class="{$field_id}-{$index}-choice-type-box">
<h4>Choices</h4>
                <table id="choices-{$field_id}-{$index}">
                    <thead>
                        <tr>
                            <th></th> 
                            <th width="50%">Label</th>
                            <th></th>                            
                        </tr>
                    </thead>
                    <tbody>

HTML;

$choice_n = 0;
if( $quiz['choices'] ) {
foreach($quiz['choices'] as $choice) {

$is_correct = ( isset($choice['correct']) ) ? 'CHECKED' : '';

echo <<<HTML
                    <tr id="quiz-choice-{$index}-{$choice_n}">
                        <td><a href="javascript:void(0);" class="choice-delete quiz-choice-delete" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$choice_n}"><div class="dashicons dashicons-no-alt"></div></a></td>
                        <td><input type="text" class="block input-popup-title" name="{$field_id}[{$index}][choices][{$choice_n}][choice]" value="{$choice['choice']}"></td>
                        <td><input type="checkbox" name="{$field_id}[{$index}][choices][{$choice_n}][correct]" value="1" $is_correct>Correct Answer?</td>
                    </tr>
HTML;
$choice_n++;
}
}
echo <<<HTML
                </tbody>
                </table>
                
                <a href="javascript:void(0);" data-id="{$this->id}" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$choice_n}" class="add-choice-item button button-primary button-large">Add Choice</a>
</div>
<div id="{$field_id}-{$index}-input" style="display:{$input_type_input_display}" class="{$field_id}-{$index}-choice-type-box">
<h4>Input Boxes</h4>

  <table id="inputbox-{$field_id}-{$index}">
                    <thead>
                        <tr>
                            <th></th> 
                            <th>Box Type</th> 
                            <th>Label / Placeholder</th>
                            <th width="100%">Answers / Choices</th>                            
                        </tr>
                    </thead>
                    <tbody>

HTML;

$input_n = 0;
if( $quiz['input'] ) {
foreach($quiz['input'] as $input) {

$type_options = '';
foreach(array('input', 'label', 'dropdown') as $type_opt) {
        $type_options .= '<option ';
        if($type_opt == $input['type']) $type_options .= 'SELECTED';
        $type_options .= '>'.$type_opt.'</option>';
}

echo <<<HTML
                    <tr id="quiz-input-{$index}-{$input_n}">
                        <td><a href="javascript:void(0);" class="input-delete quiz-input-delete" data-field-id="{$field_id}" data-index="{$index}" data-n="{$input_n}"><div class="dashicons dashicons-no-alt"></div></a></td>
                        <td><select data-field-id="{$field_id}" data-index="{$index}" data-n="{$input_n}" name="{$field_id}[{$index}][input][{$input_n}][type]" class="input-box-type">{$type_options}</select></td>
                        <td><input type="text" class="block input-popup-title" name="{$field_id}[{$index}][input][{$input_n}][label]" value="{$input['label']}"></td>
HTML;
switch($input['type']) {
        default:
echo <<<HTML
                       <td class="answers-choices"><input type="text" class="block input-popup-title" name="{$field_id}[{$index}][input][{$input_n}][answer]" value="{$input['answer']}"></td>
                    
HTML;
        break;
        case 'dropdown':
echo <<<HTML
                       <td class="answers-choices">
                       <div id="dropdown-choices-{$index}-{$input_n}" class="dropdown-choices-box">
HTML;
$dn = 0;
if( count($input['choices']) > 0 ) { 
        foreach($input['choices'] as $dropdown_choice) {
                $dn_checked = ($input['answer'] == $dropdown_choice['option']) ? 'CHECKED' : '';
echo <<<HTML
                               <div id="input-dropdown-choice-{$index}-{$input_n}-{$dn}" class="input-dropdown-choice"><input data-n="{$input_n}" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$dn}" class="input-dropdown-choice-textbox" type="text" class="" name="{$field_id}[{$index}][input][{$input_n}][choices][{$dn}][option]" value="{$dropdown_choice['option']}">
                               <input id="input-dropdown-choice-radio-{$index}-{$input_n}-{$dn}" type="radio" name="{$field_id}[{$index}][input][{$input_n}][answer]" value="{$dropdown_choice['option']}" {$dn_checked}> Correct?
                               <button class="btn btn-success dropdown-choices-close" type="button" data-n="0" data-n="{$input_n}" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$dn}">x</button>
                               </div>
HTML;
$dn++;
}

}
echo <<<HTML
                       </div>
                       
                       <button class="button button-primary button-large dropdown-choices-add" type="button" data-choice="{$dn}" data-n="{$input_n}" data-field-id="{$field_id}" data-index="{$index}">Add Choice</button>
                       
                       </td>
HTML;
        break;
}
echo "</tr>";
$input_n++;
}
}
echo <<<HTML
                </tbody>
                </table>
                
<a href="javascript:void(0);" data-id="{$this->id}" data-field-id="{$field_id}" data-index="{$index}" data-n="{$input_n}" class="add-input-item button button-primary button-large">Add Box</a>
</div>                
                </div>
                <br class="clear" />
            </div>
      </li>
HTML;
$index++;
}
}
echo <<<HTML
</ul>
<a href="javascript:void(0);" id="add-quiz-item" data-id="{$this->id}" data-field-id="{$field_id}" data-index="{$count_index}" class="button button-primary button-large">Add Item</a>
HTML;
            
        }
     */
    }
}
