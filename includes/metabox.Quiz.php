<?php

if ( class_exists( 'Custom_Metabox' ) ) {
	class Video_Lesson_Quiz_Metabox extends Custom_Metabox {
		function meta_box_content() {
			global $post;
			$field_id = 'lesson_video_quizzes';
			$current_value = get_post_meta($post->ID, 'lesson_video_quizzes', true);
           $count_index = count( $current_value );
echo <<<HTML
 <ul id="{$this->id}-sortable">
HTML;
$index = 0;
if( ($current_value) && count( $current_value ) > 0) {
foreach($current_value as $quiz ) {

$input_type_choices = ($quiz['input_type'] == 'choices') ? 'CHECKED' : '';
$input_type_choices_display = ($quiz['input_type'] == 'choices') ? 'block' : 'none';

$input_type_input = ($quiz['input_type'] == 'input') ? 'CHECKED' : '';
$input_type_input_display = ($quiz['input_type'] == 'input') ? 'block' : 'none';

$input_type_choices_single = 'CHECKED';
if($quiz['choices_type'] == 'multiple') {
	$input_type_choices_multiple = 'CHECKED';
	$input_type_choices_single = '';
} 
echo <<<HTML
      <li id="video-quiz-{$index}" class="">
            <div class="quiz-item-bar">
                <div class="quiz-item-handle">
                    <strong><span id="popupTime-{$index}">{$quiz['popupTime']}</span> - <span id="popupTitle-{$index}">{$quiz['popupTitle']} <small>({$quiz['input_type']})</small></span></strong> 
                    <div class="item-actions">
                        <a href="javascript:void(0);" class="item-edit quiz-item-edit" data-id="{$index}"><div class="dashicons dashicons-edit"></div></a>
                        <a href="javascript:void(0);" class="item-delete quiz-item-delete" data-id="{$index}"><div class="dashicons dashicons-no-alt"></div></a>
                    </div>
                </div>
            </div>
            <div id="quiz-item-settings-{$index}" class="quiz-item-settings">
                 <p>
                    <label>Popup Time (MM:SS)<br>
                        <input type="text" name="{$field_id}[{$index}][popupTime]" value="{$quiz['popupTime']}" class="block input-popup-time" id="input-popup-time-{$index}" data-id="{$index}">
                    </label>
                </p>            
                <p>
                    <label>Question<br>
                        <input type="text" name="{$field_id}[{$index}][popupTitle]" value="{$quiz['popupTitle']}" class="block input-popup-title" id="input-popup-title-{$index}" data-id="{$index}">
                    </label>
                </p>
                <div class="choice">
                <h3>
                <label><input {$input_type_choices} class="choice-type choice-type-{$index}" data-class="{$field_id}-{$index}-choice-type-box" data-id="{$field_id}-{$index}-choice" type="radio" value="choices" name="{$field_id}[{$index}][input_type]"> Multiple Choice</label>
                <label><input {$input_type_input} class="choice-type choice-type-{$index}" data-class="{$field_id}-{$index}-choice-type-box" data-id="{$field_id}-{$index}-input" type="radio" value="input" name="{$field_id}[{$index}][input_type]"> Input Box</label></h3>
<div id="{$field_id}-{$index}-choice" style="display:{$input_type_choices_display}" class="{$field_id}-{$index}-choice-type-box">
<h4>Choices</h4>

                <label><input {$input_type_choices_single} class="choice-type choice-type-{$index}" data-class="{$field_id}-{$index}-choice-type-box-choices-type" data-id="{$field_id}-{$index}-choice-type" type="radio" value="single" name="{$field_id}[{$index}][choices_type]"> Single Answer</label>
                <label><input {$input_type_choices_multiple} class="choice-type choice-type-{$index}" data-class="{$field_id}-{$index}-choice-type-box-choices-type" data-id="{$field_id}-{$index}-choices-type" type="radio" value="multiple" name="{$field_id}[{$index}][choices_type]"> Multiple Answer</label>
                
                <table id="choices-{$field_id}-{$index}">
                    <thead>
                        <tr>
                            <th></th> 
                            <th width="50%">Label</th>
                            <th></th>                            
                        </tr>
                    </thead>
                    <tbody>

HTML;

$choice_n = 0;
if( $quiz['choices'] ) {
foreach($quiz['choices'] as $choice) {

$is_correct = ( isset($choice['correct']) ) ? 'CHECKED' : '';

echo <<<HTML
                    <tr id="quiz-choice-{$index}-{$choice_n}">
                        <td><a href="javascript:void(0);" class="choice-delete quiz-choice-delete" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$choice_n}"><div class="dashicons dashicons-no-alt"></div></a></td>
                        <td><input type="text" class="block input-popup-title" name="{$field_id}[{$index}][choices][{$choice_n}][choice]" value="{$choice['choice']}"></td>
                        <td><input type="checkbox" name="{$field_id}[{$index}][choices][{$choice_n}][correct]" value="1" $is_correct>Correct Answer?</td>
                    </tr>
HTML;
$choice_n++;
}
}
echo <<<HTML
                </tbody>
                </table>
                
                <a href="javascript:void(0);" data-id="{$this->id}" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$choice_n}" class="add-choice-item button button-primary button-large">Add Choice</a>
</div>
<div id="{$field_id}-{$index}-input" style="display:{$input_type_input_display}" class="{$field_id}-{$index}-choice-type-box">
<h4>Input Boxes</h4>

  <table id="inputbox-{$field_id}-{$index}">
                    <thead>
                        <tr>
                            <th></th> 
                            <th>Box Type</th> 
                            <th>Label / Placeholder</th>
                            <th width="100%">Answers / Choices</th>                            
                        </tr>
                    </thead>
                    <tbody>

HTML;

$input_n = 0;
if( $quiz['input'] ) {
foreach($quiz['input'] as $input) {

$type_options = '';
foreach(array('input', 'label', 'dropdown') as $type_opt) {
        $type_options .= '<option ';
        if($type_opt == $input['type']) $type_options .= 'SELECTED';
        $type_options .= '>'.$type_opt.'</option>';
}

echo <<<HTML
                    <tr id="quiz-input-{$index}-{$input_n}">
                        <td><a href="javascript:void(0);" class="input-delete quiz-input-delete" data-field-id="{$field_id}" data-index="{$index}" data-n="{$input_n}"><div class="dashicons dashicons-no-alt"></div></a></td>
                        <td><select data-field-id="{$field_id}" data-index="{$index}" data-n="{$input_n}" name="{$field_id}[{$index}][input][{$input_n}][type]" class="input-box-type">{$type_options}</select></td>
                        <td><input type="text" class="block input-popup-title" name="{$field_id}[{$index}][input][{$input_n}][label]" value="{$input['label']}"></td>
HTML;
switch($input['type']) {
        default:
echo <<<HTML
                       <td class="answers-choices"><input type="text" class="block input-popup-title" name="{$field_id}[{$index}][input][{$input_n}][answer]" value="{$input['answer']}"></td>
                    
HTML;
        break;
        case 'dropdown':
echo <<<HTML
                       <td class="answers-choices">
                       <div id="dropdown-choices-{$index}-{$input_n}" class="dropdown-choices-box">
HTML;
$dn = 0;
if( count($input['choices']) > 0 ) { 
        foreach($input['choices'] as $dropdown_choice) {
                $dn_checked = ($input['answer'] == $dropdown_choice['option']) ? 'CHECKED' : '';
echo <<<HTML
                               <div id="input-dropdown-choice-{$index}-{$input_n}-{$dn}" class="input-dropdown-choice"><input data-n="{$input_n}" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$dn}" class="input-dropdown-choice-textbox" type="text" class="" name="{$field_id}[{$index}][input][{$input_n}][choices][{$dn}][option]" value="{$dropdown_choice['option']}">
                               <input id="input-dropdown-choice-radio-{$index}-{$input_n}-{$dn}" type="radio" name="{$field_id}[{$index}][input][{$input_n}][answer]" value="{$dropdown_choice['option']}" {$dn_checked}> Correct?
                               <button class="btn btn-success dropdown-choices-close" type="button" data-n="0" data-n="{$input_n}" data-field-id="{$field_id}" data-index="{$index}" data-choice="{$dn}">x</button>
                               </div>
HTML;
$dn++;
}

}
echo <<<HTML
                       </div>
                       
                       <button class="button button-primary button-large dropdown-choices-add" type="button" data-choice="{$dn}" data-n="{$input_n}" data-field-id="{$field_id}" data-index="{$index}">Add Choice</button>
                       
                       </td>
HTML;
        break;
}
echo "</tr>";
$input_n++;
}
}
echo <<<HTML
                </tbody>
                </table>
                
<a href="javascript:void(0);" data-id="{$this->id}" data-field-id="{$field_id}" data-index="{$index}" data-n="{$input_n}" class="add-input-item button button-primary button-large">Add Box</a>
</div>                
                </div>
                <br class="clear" />
            </div>
      </li>
HTML;
$index++;
}
}
echo <<<HTML
</ul>
<a href="javascript:void(0);" id="add-quiz-item" data-id="{$this->id}" data-field-id="{$field_id}" data-index="{$count_index}" class="button button-primary button-large">Add Item</a>
HTML;
            
        }
	}
}
