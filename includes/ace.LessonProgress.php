<?php

if( !function_exists('aceLessonProgressInsert') ) {
    function aceLessonProgressInsert($user_id, $lesson_id, $date_taken) {
        global $wpdb;
        $sql = sprintf("INSERT INTO `%sace_lesson_progress` (`id`, `user_id`, `lesson_id`, `date_taken`) VALUES (NULL, '%d', '%d', '%s');", $wpdb->prefix, $user_id, $lesson_id, $date_taken);
        $wpdb->query($sql);
    }
}

if( !function_exists('aceLessonProgressUpdate') ) {
    function aceLessonProgressUpdate($user_id, $lesson_id, $date_taken) {
        global $wpdb;
        $sql = sprintf("UPDATE `%sace_lesson_progress` SET `date_taken` = '%s' WHERE `user_id` = '%d' AND `lesson_id` = '%d';", $wpdb->prefix, $date_taken, $user_id, $lesson_id);
        $wpdb->query($sql);
    }
}

if( !function_exists('aceLessonProgressRetrieve') ) {
    function aceLessonProgressRetrieve($user_id, $lesson_id) {
        global $wpdb;
        $sql = sprintf("SELECT * FROM `%sace_lesson_progress` WHERE `user_id` = '%d' AND `lesson_id` = '%d';", $wpdb->prefix, $user_id, $lesson_id);
        $result = $wpdb->get_results($sql);
        
        return $result;
    }
}

if( !function_exists('aceLessonProgressByUser') ) {
    function aceLessonProgressByUser($user_id) {
        global $wpdb;
        $sql = sprintf("SELECT * FROM `%sace_lesson_progress` WHERE `user_id` = '%d' ORDER BY `date_taken` DESC;", $wpdb->prefix, $user_id);
        $result = $wpdb->get_results($sql);
        
        return $result;
    }
}

add_action( 'wp_ajax_video_finished', 'aceLessonProgressVideoFinished' );
function aceLessonProgressVideoFinished() {
    global $current_user;
    get_currentuserinfo();
    if ( isset( $_POST['lesson_id']) ) {
        $lesson_id = $_POST['lesson_id'];
        $user_record = aceLessonProgressRetrieve( $current_user->ID, $lesson_id  );
            if( count( $user_record ) > 0 ) {
                aceLessonProgressUpdate($current_user->ID, $lesson_id , date('Y-m-d H:i:s'));
            } else {
                aceLessonProgressInsert($current_user->ID, $lesson_id , date('Y-m-d H:i:s'));
            }
        echo "Congratulations! You have completed the lesson! You got an award!";
        exit;
    }
    echo "Not recorded!";
    exit;
}
