<?php
/* 
Name: AceMyMath RPG Class 
Filename: AceMyMathRPG.php
*/

if ( ! class_exists('AceMyMathRPG') ) 
{
    class AceMyMathRPG {
        function __construct()
        {
            //$this->create_post_type_problem();
            $this->create_post_type_testimonials();
            
            add_action( 'admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));
            add_action('admin_print_footer_scripts', array(&$this, 'admin_footer'), 9999);
            add_action( 'admin_enqueue_scripts', array(&$this, 'admin_style') );
            
            add_action( 'admin_menu', array(&$this,'main_admin_menu') );
            add_action( 'admin_init', array(&$this,'main_admin_name_change') );
            add_action( 'admin_init', array(&$this,'create_chapter_meta') );
            add_action( 'admin_init', array(&$this,'create_level_meta') );
            
            if ( class_exists('AceMyMathLessons') ) new AceMyMathLessons;
            if ( class_exists('AceMyMathCharacters') ) new AceMyMathCharacters;
            if ( class_exists('AceMyMathSettings') ) new AceMyMathSettings;
            if ( class_exists('AceMyMathMembership') ) new AceMyMathMembership;

        }
        
                
        function create_post_type_problem() {
            if ( class_exists('Custom_Post_Type') ) {
                $problems = new Custom_Post_Type('problem', 'Problem', 'Problems', '', 1002);
                $problems->init();
                
                if ( class_exists('Custom_Taxonomy') ) {
                    $level = new Custom_Taxonomy('problem', 'level');
                    $level->init();
                    
                    $difficulty = new Custom_Taxonomy('problem', 'difficulty', 'Difficulty', 'Difficulty', 'Difficulties');
                    $difficulty->set_hierarchical(false)->init();
                }
            }
        }   
            
        function create_post_type_testimonials() {
            if ( class_exists('Custom_Post_Type') ) {
                $testi = new Custom_Post_Type('testimonial', 'Testimonial', 'Testimonials', 'Testimonials', 1003);
                $testi->add_support('editor' , 'thumbnail');
                $testi->init();                
            }
        }   
        
       function create_chapter_meta() {
                    if ( class_exists('Custom_Term_Meta') ) {
                        $chapter_meta = new Custom_Term_Meta('chapter');
                       $levels_raw = get_terms('level', 'hide_empty=0');
                        $levels = array();

                        foreach($levels_raw as $lvlraw) {   
                            $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
                            if( isset( $levels[$order]) ) {
                                $levels[] = $lvlraw;
                            } else {
                                $levels[$order] = $lvlraw;
                            }
                        }

                        ksort($levels);
                        $chapter_meta->add_field(array('label' => 'Menu Order', 'meta_key'=> 'menu_order', 'meta_value'=> '0', 'desc'=> 'Menu Order'));
                       
                        foreach( $levels as $level_term) {
                            $chapter_meta->add_field( array('label' => $level_term->name . ' Title', 'meta_key'=> 'chapter_title_' . $level_term->term_id, 'meta_value'=> '', 'desc'=>'This is the title for this chapter for '. $level_term->name .' level.') );
                        }
                        
                        $chapter_meta->init();
                    }
       }
       
       function create_level_meta() {
                    if ( class_exists('Custom_Term_Meta') ) {
                        $level_meta = new Custom_Term_Meta('level');
                        $level_meta->add_field(array('label' => 'Menu Order', 'meta_key'=> 'menu_order', 'meta_value'=> '0', 'desc'=> 'Menu Order'));
                        $level_meta->init();
                    }
       }
       
       function main_admin_menu()
       {
            add_menu_page( 'Ace My Math RPG', 'Ace Game', 'manage_options', 'acemymath', array(&$this, 'main_admin_page'), plugins_url( 'images/sword-icon.png', dirname(__FILE__) ), 1000 ); 
       }
       
       function main_admin_page()
       {
         echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		        echo '<h2>Dashboard</h2>';
	        echo '</div>';
       }
       
       function main_admin_name_change() {
            $GLOBALS['submenu']['acemymath'][0][0] = 'Dashboard';
       }
       
       function admin_enqueue_scripts() {
            wp_enqueue_script( 'jquery-ui-tabs' );
            wp_enqueue_script( 'jquery-ui-accordion' );
            wp_enqueue_script( 'jquery-ui-sortable' );
            wp_enqueue_script( 'ace_custom_metabox', plugins_url( 'js/ace_custom_metabox.js', dirname(__FILE__)  ), true );
            wp_enqueue_script( 'ace_membership_plans', plugins_url( 'js/ace_membership_plans.js', dirname(__FILE__)  ), true );
       }
       
       function admin_footer() {
echo <<<HTML
    <script type="text/javascript">
    <!--
        (function($) {
            $( ".metabox-tabs" ).tabs({ active: 0 });
            $( ".metabox-sortable" ).sortable({
                  placeholder: "item-placeholder"
                });
            $( ".metabox-sortable" ).disableSelection();
        })(jQuery);
    -->
    </script>
HTML;
         }
         
        function admin_style() {
            wp_register_style( 'jquery-ui-smoothness', '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css', false, '1.10.4' );
            wp_enqueue_style( 'jquery-ui-smoothness' );
            wp_register_style( 'ace_custom_metabox', plugins_url( 'css/ace_custom_metabox.css', dirname(__FILE__)  ) );
            wp_enqueue_style( 'ace_custom_metabox' );
            wp_register_style( 'ace_membership_plans', plugins_url( 'css/ace_membership_plans.css', dirname(__FILE__)  ) );
            wp_enqueue_style( 'ace_membership_plans' );
        }

    }
    
}
