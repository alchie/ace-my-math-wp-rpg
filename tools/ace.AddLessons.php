<?php

if ( ( ! class_exists('AceMyMathAddLessons') ) && ( class_exists('AceMyMathRPG')) ) 
{
    class AceMyMathAddLessons {
        
        function __construct() {
            add_action('admin_menu', array(&$this, 'admin_menu') );  
            add_action('admin_print_footer_scripts', array(&$this, 'admin_footer'), 9998 );       
        }
        
        function admin_menu() {
             add_menu_page( 'Ace Tools', 'Ace Tools', 'manage_options', 'acemymathtools', array(&$this, 'admin_page'), plugins_url( 'images/tools.png', dirname(__FILE__) ) ); 
            //add_menu_page( 'acemymath', 'Add Lessons Tool', 'Add Lessons', 'manage_options', 'acemymath-addlessons', array(&$this, 'admin_page') ); 
        }
        
        function admin_page() {
             echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
            switch($_GET['tool']) {
	            default:
		                echo '<h2>Ace Tools</h2>';
		                echo '<a href="admin.php?page=acemymathtools&tool=easy-add-lessons">Easy Add Lessons</a>';
	            break;
	            case 'easy-add-lessons':

echo '<h2>Easy Add Lessons</h2>';

$chapter_name = $level_name = $chapter_id = $level_id = '';

$chapters_raw = get_terms('chapter', 'hide_empty=0');
$chapters = array();
foreach($chapters_raw as $chptraw) {   
    $order = (int) get_custom_termmeta($chptraw->term_id, 'menu_order', true);
    if( isset( $chapters[$order] ) ) {
        $chapters[] = $chptraw;
    } else {
        $chapters[$order] = $chptraw;
    }
}
ksort($chapters); 

$levels_raw = get_terms('level', 'hide_empty=0');
$levels = array();

foreach($levels_raw as $lvlraw) {   
    $order = (int) get_custom_termmeta($lvlraw->term_id, 'menu_order', true);
    if( isset( $levels[$order]) ) {
        $levels[] = $lvlraw;
    } else {
        $levels[$order] = $lvlraw;
    }
}
ksort($levels);

$start = ( $_GET['start'] != '' || $_GET['start'] > 0 ) ? $_GET['start'] : 1;
$end = ( $_GET['end'] != '' || $_GET['end'] > 0 ) ? $_GET['end'] : 10;

echo '
<hr>
<div id="col-container">
<div id="col-left" style="float:left">
<form method="get">
<input type="hidden" name="page" value="acemymathtools">
<input type="hidden" name="tool" value="easy-add-lessons">';
/*
<p>Start<br> <input type="text" name="start" value="'.$start.'"></p>
<p>End<br> <input type="text" name="end" value="'.$end.'"></p>';
*/
echo '<p>Grade Level <br><select name="level">';
echo '<option value=""> - - Select a Level - - </option>';
foreach( $levels as $level ) {
    $level_current = (isset($_GET['level']) && $_GET['level'] == $level->term_id ) ? 'SELECTED' : '';
    if (isset($_GET['level']) && $_GET['level'] == $level->term_id ) {
        $level_name  = $level->name ;
        $level_id = $level->term_id;
      }
    echo '<option value="'.$level->term_id.'" '.$level_current.'>'.$level->name.'</option>';
}
echo '</select></p>';
/*
echo '<p>Chapter <br><select name="chapter">';
echo '<option value=""> - - Select a Chapter - - </option>';

foreach( $chapters as $chapter ) {
    $chapter_current = (isset($_GET['chapter']) && $_GET['chapter'] == $chapter->term_id ) ? 'SELECTED' : '';
    if (isset($_GET['chapter']) && $_GET['chapter'] == $chapter->term_id ) {
        $chapter_name  = $chapter->name ;
        $chapter_id = $chapter->term_id;
        }
    
    echo '<option value="'.$chapter->term_id.'" '.$chapter_current.'>'.$chapter->name.'</option>';
}
echo '</select></p>';
*/
echo '<input type="submit" value="Generate"></form></div>';

if( 
//( isset($_GET['chapter']) && $_GET['chapter'] != '' ) && 
( isset($_GET['level']) && $_GET['level'] != '' ) 
//&& (isset($_GET['start'])  && $_GET['start'] != '' )
//&& (isset($_GET['end']) && $_GET['end'] != '' ) 
) { 

if( ( isset($_GET['level']) && $_GET['level'] != '' ) && isset($_POST['lessons']) && (count($_POST['lessons']) > 0 )) {
    
    foreach( $_POST['lessons'] as $chapter_id => $lessons ) {
        foreach($lessons as $order=>$title) {
            if( $title != '') { 
                $post_id = wp_insert_post(array(
                    'post_title' => $title,
                    'post_type' => 'lesson',
                    'menu_order' => $order,
                    'tax_input' => array('chapter'=>$chapter_id, 'level'=>$_GET['level']),
                    'comment_status'=> 'closed',
                    'post_status'=>'publish',
                ));
            }
        }
    }

}

if ( isset($_POST['chapter_title']) && ( count($_POST['chapter_title']) > 0 ) ) {
    foreach( $_POST['chapter_title'] as $id => $value ) {
        update_custom_termmeta($id, 'chapter_title_' .$_GET['level'], $value );
    }
}


$order=$start;


echo <<<HTML
<div id="col-right">
<h2>{$level_name}</h2>
<form method="POST" action="">
<div id="chapters-accordion">
HTML;

foreach( $chapters as $chapter ) {
$chapter_title = ($_GET['level'] != '') ? get_custom_termmeta($chapter->term_id, 'chapter_title_' .$_GET['level'], true ) : '';
echo '<h3 id="chapter-title-'.$chapter->term_id.'">'.$chapter->name.' <a href="#" style="float:right" class="delete-chapter" data-chapter-id="'.$chapter->term_id.'">X</a> </h3>
  <div id="chapter-body-'.$chapter->term_id.'">
    <p>
   <p>Chapter Title:<br><input type="text" name="chapter_title['.$chapter->term_id.']" value="'.$chapter_title.'" style="width:100%"></p>
<hr>
<p>Lesson 1: <br><input type="text" name="lessons['.$chapter->term_id.'][1]" value="" style="width:100%"></p>
<input type="button" class="button button-primary add-chapter" data-n="1" data-chapter-id="'.$chapter->term_id.'" value="Add Lesson">
    </p>
  </div>';
}

echo <<<HTML
</div>
<p>
<input type="submit" class="button button-primary" value="Add Lessons"></p>
</form>
HTML;
} // start end

	            break;
	        }
	        echo '</div></div></div> ';
        }
        function admin_footer() {
            echo '<script>
  (function($) {
    $( "#chapters-accordion" ).accordion({ heightStyle: "content" });
    $( ".delete-chapter" ).click(function() {
        var chapter = $(this).attr("data-chapter-id");
        $("#chapter-title-" + chapter).remove();
        $("#chapter-body-" + chapter).remove();   
        return false;     
    });
    $( ".add-chapter" ).click(function() {
        var n = parseInt( $(this).attr("data-n") ) + 1;
        var chapter = $(this).attr("data-chapter-id");
        $(this).before(\'<p>Lesson \'+n+\': <br><input type="text" name="lessons[\'+chapter+\'][\'+n+\']" value="" style="width:100%"></p>\');
        $(this).attr("data-n", n);
    });
  })(jQuery);
  </script>';
        }
        
    }
    new AceMyMathAddLessons();

}
