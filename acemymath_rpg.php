<?php
/*
Plugin Name: Ace My Math RPG
Plugin URI: http://www.acemymath.com/
Description: Math RPG Game
Author: Chester Alan
Version: 1.0
Author URI: http://www.acemymath.com/
*/

require('includes/lib.Custom_Post_Type.php');
require('includes/lib.Custom_Taxonomy.php');
require('includes/lib.Custom_Metabox.php');
require('includes/lib.Custom_Term_Meta.php');
require('includes/ace.Main.php');
require('includes/ace.Lessons.php');
require('includes/ace.Characters.php');
require('includes/ace.Settings.php');
require('includes/ace.Membership.php');
require('includes/ace.LessonProgress.php');
//require('tools/ace.AddLessons.php');
require('includes/metabox.Quiz.php');

if ( class_exists('AceMyMathRPG') ) {
    new AceMyMathRPG();
}

function AceMyMathRPG_activation()
{
     add_role(
        'parent',
        __( 'Parent' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => false,
            'delete_posts' => false, // Use false to explicitly deny
        )
    );
    
    add_role(
        'student',
        __( 'Student' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => false,
            'delete_posts' => false, // Use false to explicitly deny
        )
    );
    
    update_option('default_role', 'parent');
    
    global $wpdb;
    $sql[] = "CREATE TABLE `{$wpdb->prefix}ace_rpg_characters` (
             `ID` bigint(20) NOT NULL AUTO_INCREMENT,
             `user_id` bigint(20) NOT NULL,
             `character_id` int(10) NOT NULL,
             `character_name` varchar(100) NOT NULL,
             PRIMARY KEY (`ID`),
             UNIQUE KEY `character_name` (`character_name`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
    
    $sql[] = "CREATE TABLE `{$wpdb->prefix}termmeta` (
         `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
         `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
         `meta_key` varchar(255) DEFAULT NULL,
         `meta_value` longtext,
         PRIMARY KEY (`meta_id`),
         KEY `term_id` (`term_id`),
         KEY `meta_key` (`meta_key`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
    
    $sql[] = "CREATE TABLE  `{$wpdb->prefix}ace_lesson_progress` (
         `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ,
         `user_id` BIGINT( 20 ) NOT NULL ,
         `lesson_id` BIGINT( 20 ) NOT NULL ,
         `date_taken` DATETIME NOT NULL ,
        PRIMARY KEY (  `id` )
        )  ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1; ";
    
    $sql[] = "CREATE TABLE `{$wpdb->prefix}ace_membership_plans` (
 `plan_id` int(10) NOT NULL AUTO_INCREMENT,
 `plan_name` varchar(200) NOT NULL,
 `plan_level` int(10) DEFAULT '0',
 `description` text,
 `levels` text,
 `lessons` text,
 `problems` text,
 `worksheets` text,
 PRIMARY KEY (`plan_id`)
)  ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
    
    $sql[] = "CREATE TABLE `{$wpdb->prefix}ace_sessions` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `ip` varchar(100) NOT NULL,
 `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    foreach($sql as $s ) {
        dbDelta( $s );
    }
    
}
register_activation_hook( __FILE__, 'AceMyMathRPG_activation' );
